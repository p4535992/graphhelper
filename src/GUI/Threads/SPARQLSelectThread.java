/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.Threads;

import GUI.MainGUI;
import Graph.Implementations.SPARQL_ResultSet;
import javax.swing.SwingWorker;

/**
 *
 * @author Elipson
 */
public class SPARQLSelectThread extends SwingWorker<SPARQL_ResultSet, Void> {

    private final String myQuery;
    private final String myModel;

    public SPARQLSelectThread(String _query, String _model) {
        myQuery = _query;
        myModel = _model;
    }

    @Override
    protected SPARQL_ResultSet doInBackground() throws Exception {
        SPARQL_ResultSet result = MainGUI.myData.RunSelect(myQuery, myModel);
        return result;
    }

    protected void done() {
        try {
            SPARQL_ResultSet result = get();
            MainGUI.WriteToLog("Expression resulted in " + result.myData.size() + " units");
            MainGUI.WriteSPARQLResult(result);
        } catch (Exception e) {
        }
    }
}
