/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.Threads;

import Graph.InterestingSearch.RDFTreeNode;
import Graph.InterestingSearch.WordScorer;
import com.google.common.collect.Multimap;
import java.util.Collection;
import java.util.Set;
import javax.swing.SwingWorker;
import org.apache.commons.collections4.MultiMap;

/**
 *
 * @author Heidi
 */
public class ScoreTreeNodesThread extends Thread {

    private final Multimap<String, String> myLiterals;
    private final Set<RDFTreeNode> myNodes;
    private final WordScorer myScorer;

    public ScoreTreeNodesThread(Multimap<String, String> _literals, Set<RDFTreeNode> _newNodes, WordScorer _scorer) {
        myLiterals = _literals;
        myNodes = _newNodes;
        myScorer = _scorer;
    }

    @Override
    public void run() {
        for (RDFTreeNode node : myNodes) {
            //get literals for node
            Collection<String> myCollection;
            myCollection = myLiterals.get(node.getNodeURI());
            // calculate score for node

            double result = myScorer.ScoreDocuments(myCollection);
            // set node score
            node.setMyMatchScore(result);

        }
    }
}
