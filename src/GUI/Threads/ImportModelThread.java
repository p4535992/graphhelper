/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.Threads;

import GUI.MainGUI;
import static GUI.MainGUI.myData;
import Graph.Enumerator.FileFormat;
import javax.swing.SwingWorker;
import Graph.Enumerator.ImportModel;

public class ImportModelThread extends SwingWorker<ImportModel, Object> {
    private final String path;
    private final String modelName;
    private final FileFormat format;
    
    public ImportModelThread (String _path, String _modelName, FileFormat _format) {
        path = _path;
        modelName = _modelName;
        format = _format;
    }
    @Override
    protected ImportModel doInBackground() throws Exception {
        ImportModel message = myData.ImportModel(
                path, modelName, format);
        return message;
    }

    @Override
    protected void done() {
        try {
            ImportModel message = (ImportModel)get();
            
            switch(message) {
            case Success:
                MainGUI.WriteToLog(modelName + " loaded.");
                MainGUI.UpdateLocalModels();
                break;
            case FileNotFound:
                MainGUI.WriteToLog(modelName + " not found");
                break;
            case FileParseError:
                MainGUI.WriteToLog(modelName + " not a valid grapg file");
                break;
            default:
                MainGUI.WriteToLog("An unknown error has occurred while loading " + modelName);
                break;
        }
        } catch(Exception e) {
            MainGUI.WriteToLog("An unknown error has occurred while loading " + this.modelName);
        }
    }
}
