/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.Threads;

import GUI.MainGUI;
import Graph.Enumerator.InterestingResult;
import Graph.Enumerator.NodeType;
import Graph.Implementations.TripleStatementWrapper;
import java.util.List;
import javax.swing.SwingWorker;

/**
 *
 * @author Elipson
 */
public class InterestingGraphThread extends SwingWorker<TripleStatementWrapper, String> {

    private final String myModel;
    private final String myBagOfWords;
    private final int myMaxNodes;
    private final int myMaxDist;
    private final String myCenter;
    private final NodeType myCenterType;
    private final double myBeta;
    private final String[] myFilters;
    private final String myLanguage;
    
    public InterestingGraphThread(String _model, String _bagOfWords,
            int _maxNodes, int _maxDist, String _centerNode, 
            NodeType _centerType, double _beta, String[] _filters,
            String _lang) {
        myModel = _model;
        myBagOfWords = _bagOfWords;
        myMaxNodes = _maxNodes;
        myMaxDist = _maxDist;
        myCenter = _centerNode;
        myCenterType = _centerType;
        myBeta = _beta;
        myFilters = _filters;
        myLanguage = _lang;
    }

    @Override
    protected TripleStatementWrapper doInBackground() throws Exception {
        TripleStatementWrapper results = MainGUI.myData.
                InterestingSearch(
                        myModel,
                        myBagOfWords,
                        myMaxNodes,
                        myMaxDist,
                        myCenter,
                        myCenterType,
                        myBeta,
                        myFilters,
                        this,
                        myLanguage);
        return results;
    }

    @Override
    protected void done() {
        try {
            TripleStatementWrapper result = get();

            switch ((InterestingResult) result.actionReport) {
                case Timeout:
                    MainGUI.WriteToLog("Connection timed out while creating interesting graph");
                    break;
                case Success:
                    MainGUI.WriteToLog("Success");
                    MainGUI.WriteToLog(result.myTriples.size() + " relevant triples found");
                    MainGUI.VisualiseInterestingGraph(result.myTriples, myModel);
                    break;
                case InitialNodeNotFound:
                    MainGUI.WriteToLog("Init node not found");
                    break;
                case ModelNotFound:
                    MainGUI.WriteToLog("Model not found");
                    break;
            }
        } catch (Exception e) {
            MainGUI.WriteToLog("An unknown error occurred during creation of interesting graph");
        }
    }

    @Override
    protected void process(List<String> chunks) {
        for (String s : chunks) {
            MainGUI.WriteToLog(s);
        }
    }

    public void ReportProgress(String _msg) {
        this.publish(_msg);
    }
}
