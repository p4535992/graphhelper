/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.MenuBar;

import GUI.Dialogs.ImportGenericEndpoint;
import GUI.Threads.ImportModelThread;
import GUI.Dialogs.LoadModelDialog;
import GUI.MainGUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 *
 * @author Elipson
 */
public class MenuBar extends JMenuBar implements ActionListener {

    private JMenu myFile;

    public MenuBar() {
        this.removeAll();
        this.myFile = new JMenu("File");
        JMenuItem menu = new JMenuItem("Import Local Model");
        menu.addActionListener(this);
        this.myFile.add(menu);

        menu = new JMenuItem("Import Generic Endpoint");
        menu.addActionListener(this);
        this.myFile.add(menu);

        menu = new JMenuItem("Quit");
        menu.addActionListener(this);
        this.myFile.add(menu);

        this.add(myFile);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JMenuItem source = (JMenuItem) (e.getSource());
        switch (source.getText()) {
            case "Import Local Model":
                this.ImportModel();
                break;
            case "Import Generic Endpoint":
                ImportGenericEndpoint();
                break;
            case "Quit":
                System.out.println("Quit Selected");
                break;
        }
    }

    private void ImportModel() {
        
        LoadModelDialog myDialog = new LoadModelDialog(MainGUI.getFrame(), true);
        myDialog.setVisible(true);
        
        if(myDialog.isMyValidForm()) {
            ImportModelThread myThread = new ImportModelThread(
                    myDialog.getMyFilePath(), 
                    myDialog.getMyModelName(),
                    myDialog.getMyFileFormat());
            
            myThread.execute();
            
            if(myDialog.getMyMaxNodes() > 0) {
                MainGUI.myData.setMaxVertices(myDialog.getMyModelName(), myDialog.getMyMaxNodes());
            }
        }
    }

    /**
     * Requests URL and name for an endpoint from user.
     */
    private void ImportGenericEndpoint() {
        
        ImportGenericEndpoint dialog = new ImportGenericEndpoint(MainGUI.getFrame(), true);
        dialog.setVisible(true);
        
        if(dialog.getIsIsValid()) {
            String name = dialog.getMyName();
            String endpoint = dialog.getMyEndpoint();
            int maxV = dialog.getMyMaxShown();
            int timeout = dialog.getMyTimeout();
            
            MainGUI.myData.ImportGenericHTTPEndPoint(endpoint, name);
            MainGUI.UpdateLocalModels();
            MainGUI.myData.setMaxVertices(name, maxV);
            MainGUI.myData.setTimeout(name, timeout*1000);
        }
        
//        String newEndpoint = JOptionPane.showInputDialog(null, "Enter endpoint URL(e.g. \"http://dbpedia.org/sparql\")", "Import Generic Endpoint", 1);
//        
//        if(newEndpoint != null) {
//            
//            String newEndpointName = JOptionPane.showInputDialog(null, "Enter endpoint name", "Import Generic Endpoint", 1);
//            if(newEndpointName != null) {
//                MainGUI.myData.ImportGenericHTTPEndPoint(newEndpoint, newEndpointName);
//                MainGUI.UpdateLocalModels();
//                MainGUI.WriteToLog("New endpoint added: " + newEndpointName);
//            }
//        }
    }
}
