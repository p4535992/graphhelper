/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.TabContainer;

import Graph.Implementations.ThreadCompleteListener;
import com.hp.hpl.jena.rdf.model.Statement;
import java.util.List;
import java.util.Set;

public class Result4Frontier extends Thread {

    private final ThreadCompleteListener myMaster;
    private final List<Statement> myStatements;
    private final boolean myInboundSearch;
    private final Set<String> myFrontier;
    private final Set<String> myBlackList;

    public Result4Frontier(
            List<Statement> _list, boolean _inbound,
            Set<String> _frontier, Set<String> _blacklist,
            ThreadCompleteListener _master) {
        myStatements = _list;
        myInboundSearch = _inbound;
        myFrontier = _frontier;
        myBlackList = _blacklist;
        myMaster = _master;
    }

    @Override
    public final void run() {
        try {
            doRun();
        } finally {
            notifyListeners();
        }
    }

    private void notifyListeners() {
        myMaster.notificationThreadComplete(this);
    }

    public void doRun() {
        String t;
        if (myInboundSearch == true) {
            for (Statement s : myStatements) {
                t = s.getSubject().toString();
                if (!myBlackList.contains(t)) {
                    myBlackList.add(t);
                    if (!myFrontier.contains(t)) {
                        myFrontier.add(t);
                    }
                }
            }
        } else {
            for (Statement s : myStatements) {
                t = s.getObject().toString();
                if (!myBlackList.contains(t)) {
                    myBlackList.add(t);
                    if (!myFrontier.contains(t)) {
                        myFrontier.add(t);
                    }
                }
            }
        }
    }
}
