/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.TabContainer;

import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxUtils;
import java.awt.Color;
import java.util.Hashtable;

/**
 * This class contains static methods which define various visual styles for
 * vertices, Literals, Blank nodes and edges.
 */
public class GraphStyles {

    /**
     * Returns the default elliptical style for vertices.
     *
     * @return Hashtable containing style
     */
    public static Hashtable<String, Object> getVertex() {
        Hashtable<String, Object> style = new Hashtable<>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_RECTANGLE);
        style.put(mxConstants.STYLE_FONTCOLOR, "#774400");
        style.put(mxConstants.STYLE_STROKECOLOR, "#6482B9");
        style.put(mxConstants.STYLE_FILLCOLOR, "#C3D9FF");
        style.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
        style.put(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
        return style;
    }
    
    public static Hashtable<String, Object> getVertexWithScore() {
        Hashtable<String, Object> style = getVertex();
        if(style.contains(mxConstants.STYLE_FILLCOLOR)) {
            style.remove(mxConstants.STYLE_FILLCOLOR);
        }
        style.put(mxConstants.STYLE_FILLCOLOR, "#99FF33");
        return style;
    }
    
    public static Hashtable<String, Object> getVertexWithoutScore() {
        Hashtable<String, Object> style = getVertex();
        if (style.contains(mxConstants.STYLE_FILLCOLOR)) {
            style.remove(mxConstants.STYLE_FILLCOLOR);
        }
        style.put(mxConstants.STYLE_FILLCOLOR, "#ff9999");
        return style;
    }
    
    /**
     * Returns the default elliptical style for vertices.
     *
     * @return Hashtable containing style
     */
    public static Hashtable<String, Object> getInterestingVertex() {
        Hashtable<String, Object> style = new Hashtable<>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_RECTANGLE);
        style.put(mxConstants.STYLE_FONTCOLOR, "#774400");
        style.put(mxConstants.STYLE_STROKECOLOR, "#6482B9");
        style.put(mxConstants.STYLE_FILLCOLOR, "#C3D9FF");
        style.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
        style.put(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
        return style;
    }

    /**
     * Returns a style based on getVertex() but with a red outer line
     *
     * @return Hashtable containing style
     */
    public static Hashtable<String, Object> getVertexSelected() {
        Hashtable<String, Object> style = getInterestingVertex();
        // style.put(mxConstants.STYLE_STROKECOLOR, mxUtils.getHexColorString(Color.red));
        if (style.containsKey(mxConstants.STYLE_STROKECOLOR)) {
            style.remove(mxConstants.STYLE_STROKECOLOR);
            style.put(mxConstants.STYLE_STROKECOLOR, mxUtils.getHexColorString(Color.red));
        } else {
            style.put(mxConstants.STYLE_STROKECOLOR, mxUtils.getHexColorString(Color.red));
        }
        return style;
    }

    /**
     * Returns a style based on getVertex() but with an opacity of 30
     *
     * @return Hashtable containing style
     */
    public static Hashtable<String, Object> getVertexFaded() {
        Hashtable<String, Object> style = getInterestingVertex();
        if (style.containsKey(mxConstants.STYLE_OPACITY)) {
            style.remove(mxConstants.STYLE_OPACITY);
            style.put(mxConstants.STYLE_OPACITY, 30);
        } else {
            style.put(mxConstants.STYLE_OPACITY, 30);
        }
        return style;
    }

    /**
     * Returns the default style for literal nodes
     *
     * @return Hashtable containing style
     */
    public static Hashtable<String, Object> getLiteral() {
        Hashtable<String, Object> style = new Hashtable<>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_RECTANGLE);
        style.put(mxConstants.STYLE_FILLCOLOR, mxUtils.getHexColorString(Color.yellow));
        style.put(mxConstants.STYLE_PERIMETER, mxConstants.PERIMETER_RECTANGLE);
        style.put(mxConstants.STYLE_STROKECOLOR, mxUtils.getHexColorString(Color.black));
        style.put(mxConstants.STYLE_SPACING_LEFT, 5);
        style.put(mxConstants.STYLE_SPACING_RIGHT, 2);
        style.put(mxConstants.STYLE_SPACING_TOP, 3);
        style.put(mxConstants.STYLE_SPACING_BOTTOM, 1);
        return style;
    }

    /**
     *
     * @return
     */
    public static Hashtable<String, Object> getBlankNode() {
        Hashtable<String, Object> style = new Hashtable<>();
        style = new Hashtable<>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_ELLIPSE);
        style.put(mxConstants.STYLE_FILLCOLOR, mxUtils.getHexColorString(new Color(250, 156, 144)));
        style.put(mxConstants.STYLE_STROKECOLOR, mxUtils.getHexColorString(Color.black));
        style.put(mxConstants.STYLE_SPACING_LEFT, 2);
        style.put(mxConstants.STYLE_SPACING_RIGHT, 2);
        style.put(mxConstants.STYLE_SPACING_TOP, 2);
        style.put(mxConstants.STYLE_SPACING_BOTTOM, 2);
        return style;
    }

    /**
     * The default style for blunted edges
     *
     * @return Hashtable containing default style
     */
    public static Hashtable<String, Object> getEdgeBlunt() {
        Hashtable<String, Object> style = new Hashtable<String, Object>();
        style.put(mxConstants.STYLE_STARTARROW, mxConstants.NONE);
        style.put(mxConstants.STYLE_ENDARROW, mxConstants.NONE);
        style.put(mxConstants.STYLE_ENTRY_X, 0);
        style.put(mxConstants.STYLE_ENTRY_Y, 0.5);
        style.put(mxConstants.STYLE_EXIT_X, 1);
        style.put(mxConstants.STYLE_EXIT_Y, 0.5);
        style.put(mxConstants.STYLE_ENTRY_PERIMETER, 1);
        style.put(mxConstants.STYLE_FONTCOLOR, "#446299");
        style.put(mxConstants.STYLE_STROKECOLOR, "#6482B9");
        style.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
        style.put(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
        //style.put(mxConstants.STYLE_STROKECOLOR, mxUtils.getHexColorString(Color.black));
        return style;
    }

    /**
     * Returns a style based on getEdgeBlunt but with a stroke color of red
     *
     * @return Hashtable containing style
     */
    public static Hashtable<String, Object> getEdgeBluntSelected() {
        Hashtable<String, Object> style = getEdgeBlunt();
        if (style.containsKey(mxConstants.STYLE_STROKECOLOR)) {
            style.remove(mxConstants.STYLE_STROKECOLOR);
            style.put(mxConstants.STYLE_STROKECOLOR, mxUtils.getHexColorString(Color.red));
        } else {
            style.put(mxConstants.STYLE_STROKECOLOR, mxUtils.getHexColorString(Color.red));
        }
        return style;
    }

    /**
     * Returns a style based on getEdgeBlunt but with an opacity of 30
     *
     * @return Hashtable containing style
     */
    public static Hashtable<String, Object> getEdgeBluntFaded() {
        Hashtable<String, Object> style = getEdgeBlunt();
        if (style.containsKey(mxConstants.STYLE_OPACITY)) {
            style.remove(mxConstants.STYLE_OPACITY);
            style.put(mxConstants.STYLE_OPACITY, 30);
        } else {
            style.put(mxConstants.STYLE_OPACITY, 30);
        }
        return style;
    }
}
