/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.TabContainer;

import GUI.MainGUI;
import Graph.Implementations.SPARQL_ResultSet;
import Graph.Implementations.TripleStatement;
import Graph.InterestingPaths.InterestingPath;
import Graph.InterestingSearch.InterestingTriple;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class TabMaster extends JTabbedPane {

    private DefaultTableModel myDataModel;
    private final JTable mySPARQLTable;
    private final JScrollPane mySPARQLResults;
    private final HashMap<String, VisGraph> myGraphs = new HashMap<>();
    private final HashMap<String, InterestingGraph> myInterestinggraphs = new HashMap<>();

    public TabMaster() {
        String[] columnHeaders = {""};
        Object[][] data = {
            {"SPARQL Results will be shown here"}
        };
        myDataModel = new DefaultTableModel(data, columnHeaders);
        mySPARQLTable = new JTable(this.myDataModel);
        mySPARQLResults = new JScrollPane(mySPARQLTable);
        mySPARQLTable.setFillsViewportHeight(true);
        this.add(mySPARQLResults);
        this.addTab("SPARQL Results", mySPARQLResults);
    }

    public void UpdateSPARQLResultTab(SPARQL_ResultSet _result) {

        if (_result.validSet == true) {
            this.myDataModel = new DefaultTableModel(_result.myData, _result.myHeaders);
            this.mySPARQLTable.setModel(myDataModel);
        } else {
            MainGUI.WriteToLog(_result.errorMessage);
        }
    }

    public void AddInterestingGraph(ArrayList<InterestingTriple> _triples, String _model) {
        if (myInterestinggraphs.containsKey(_model)) {
            InterestingGraph g = myInterestinggraphs.get(_model);
            g.clearSelection();

            g.RearrangeModel();
        } else {
            InterestingGraph myGraph = new InterestingGraph(_model, _triples);
            GraphTab newTab = new GraphTab(this, _model, myGraph);
            myGraph.RearrangeModel();
            this.myInterestinggraphs.put(_model, myGraph);
        }
    }

    public void AddGraphTab(ArrayList<TripleStatement> _stmts, String _model) {
        if (this.myGraphs.containsKey(_model)) {
            VisGraph g = this.myGraphs.get(_model);
            g.Clear();
            g.UpdateGraph(_stmts);
            g.RearrangeModel();
        } else {
            VisGraph myGraph = new VisGraph(_stmts, _model);
            GraphTab newTab = new GraphTab(this, _model, myGraph);
            myGraph.RearrangeModel();
            this.myGraphs.put(_model, myGraph);
        }
    }

    public void AddInterestingPaths(String _model, InterestingPath[] _paths) {
        if (this.myGraphs.containsKey(_model)) {

        } else {
            InterestingPathsPanel myPanel = new InterestingPathsPanel(this, _model, _paths);
            InterestingTab tab = new InterestingTab(this, _model, myPanel);
        }
    }

    /**
     * Removes the custom tab _tab
     * @param _tab
     * @param _tabName 
     */
    public void RemoveCustomTab(GraphTab _tab, String _tabName) {
        this.myGraphs.remove(_tabName);
        this.remove(_tab);
    }

    /**
     * Removes the tab at _index
     * @param _index Tab to be removed
     */
    public void RemoveCustomTab(int _index) {
        this.remove(_index);
    }
}
