/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package DataSources;

import GUI.Threads.InterestingGraphThread;
import GUI.Threads.InterestingPathsThread;
import Graph.Enumerator.ChangeEdge;
import Graph.Enumerator.DeleteEdge;
import Graph.Enumerator.InsertEdge;
import Graph.Enumerator.InterestingResult;
import Graph.Enumerator.DeleteLiteral;
import Graph.Enumerator.InsertLiteral;
import Graph.Enumerator.UpdateLiteralText;
import Graph.Enumerator.NodeType;
import Graph.Enumerator.ReadyToVisualise;
import Graph.Enumerator.DeleteVertex;
import Graph.Enumerator.InsertVertex;
import Graph.Enumerator.UpdateVertexURI;
import Graph.Enumerator.VertexToBlankNode;
import Graph.Enumerator.VisualiseStatus;
import Graph.Implementations.GraphHelperUtil;
import Graph.InterestingSearch.InterestingLiterals;
import Graph.Implementations.ResourceResult;
import Graph.Implementations.SPARQL_ResultSet;
import Graph.Implementations.TripleStatement;
import Graph.Implementations.TripleStatementWrapper;
import Graph.Implementations.VisualiseWrapper;
import Graph.InterestingSearch.LiteralTriple;
import Graph.InterestingSearch.RDFTree;
import Graph.InterestingSearch.RDFTreeMatchScoreFilter;
import Graph.InterestingSearch.RDFTreeNode;
import Graph.InterestingSearch.WordScorer;
import Graph.Interfaces.DataSourceInterface;
import Graph.InterestingPaths.DisambiguityResult;
import Graph.InterestingPaths.DisambiguityResults;
import Graph.InterestingPaths.InterestingPath;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.AnonId;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP;
import com.hp.hpl.jena.util.ResourceUtils;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LocalGraph implements DataSourceInterface {

    private final Model myFullModel;
    private Model mySubModel = null;
    private String myCurrentCenter = "";
    private NodeType myCenterType;
    private int maxVertices = 300;
    private int numOfEntities = Integer.MAX_VALUE;

    public LocalGraph(Model _model) {
        this.myFullModel = _model;
        this.size();
    }

    /**
     * Performs a select query on the local model.
     *
     * @param _query Query to be executed
     * @return Returns formatted result set
     */
    @Override
    public SPARQL_ResultSet performSelect(String _query) {
        SPARQL_ResultSet resultSet = new SPARQL_ResultSet();

        try {
            Query query = QueryFactory.create(_query);

            QueryExecution qExe = QueryExecutionFactory.create(query, this.myFullModel);
            ResultSet results = qExe.execSelect();

            resultSet.myHeaders = new Vector<String>(results.getResultVars());
            int stringLength = resultSet.myHeaders.size();

            resultSet.myData = new Vector<Vector<String>>();

            while (results.hasNext()) {
                Vector<String> vector = new Vector<String>();
                QuerySolution solution = results.next();

                for (int j = 0; j < stringLength; j++) {
                    vector.add(solution.get((String) resultSet.myHeaders.get(j)).toString());
                }
                resultSet.myData.add(vector);

                resultSet.validSet = true;
            }
        } catch (Exception e) {
            resultSet.validSet = false;
            resultSet.errorMessage = e.toString();
        }
        return resultSet;
    }

    /**
     * Performs a construct query on the local model
     *
     * @param _query Query to be performed
     * @return Returns a Jena Model
     * @throws QueryExceptionHTTP Only thrown for malformed SPARQL queries
     */
    @Override
    public Model performConstruct(String _query) throws QueryExceptionHTTP {
        Model newModel = null;

        Query query = QueryFactory.create(_query);
        QueryExecution qExe = QueryExecutionFactory.create(query, this.myFullModel);

        newModel = qExe.execConstruct();
        return newModel;
    }

    /**
     * Performs a ask query on the local model
     *
     * @param _query Query to be performed
     * @return Returns true iff ask resolves as true
     * @throws QueryExceptionHTTP Only thrown for malformed SPARQL queries
     */
    @Override
    public boolean performAsk(String _query) throws QueryExceptionHTTP {
        Query myQuery = QueryFactory.create(_query);
        QueryExecution qExe = QueryExecutionFactory.create(myQuery, this.myFullModel);
        boolean result = qExe.execAsk();
        qExe.close();
        return result;
    }

    /**
     * Verifies connection for model. As model is local, method always returns
     * true
     *
     * @return Always returns true
     */
    @Override
    public boolean verifyConnection() {
        return true;
    }

    /**
     * Returns an ArrayList of triples. If no sub-model has been set, method
     * returns list for full model.
     *
     * @return Empty ArrayList or ArrayList with triples
     */
    @Override
    public ArrayList<TripleStatement> GetStatements() {
        ArrayList<TripleStatement> myStatements = new ArrayList<>();
        Model targetModel;

        // If SubModel contains a model, we use that
        if (this.mySubModel != null) {
            targetModel = this.mySubModel;
        } // else we use the full model
        else {
            targetModel = this.myFullModel;
        }

        TripleStatement temp;
        StmtIterator itr = targetModel.listStatements();

        while (itr.hasNext()) {
            temp = new TripleStatement();
            Statement stmt = itr.nextStatement();

            Resource subject = stmt.getSubject();
            temp.mySubject = subject.toString();
            if (subject.asNode().isBlank() == true) {
                temp.SubjectIsBlank = true;
                temp.mySubjectShort = "BN";
            } else {
                temp.mySubjectShort = GraphHelperUtil.cropURI(subject.toString(), myFullModel.getNsPrefixMap());
            }

            Property predicate = stmt.getPredicate();
            temp.myPredicate = predicate.toString();
            temp.myPredicateShort = GraphHelperUtil.cropURI(predicate.toString(), myFullModel.getNsPrefixMap());

            RDFNode myNode = stmt.getObject();
            temp.myObject = myNode.toString();
            if (myNode.isLiteral()) {
                temp.ObjectIsLiteral = true;
                temp.myObjectShort = GraphHelperUtil.cropLiteral(temp.myObject);
            } else if (myNode.asNode().isBlank() == true) {
                temp.myObjectShort = "BN";
                temp.ObjectIsBlank = true;
            } else {
                temp.myObjectShort = GraphHelperUtil.cropURI(myNode.toString(), myFullModel.getNsPrefixMap());
            }

            myStatements.add(temp);
        }
        return myStatements;
    }

    public ArrayList<TripleStatement> getStatements(Model _m) {
        ArrayList<TripleStatement> myStatements = new ArrayList<>();
        Model targetModel;

        TripleStatement temp;
        StmtIterator itr = _m.listStatements();

        while (itr.hasNext()) {
            temp = new TripleStatement();
            Statement stmt = itr.nextStatement();

            Resource subject = stmt.getSubject();
            temp.mySubject = subject.toString();
            if (subject.asNode().isBlank() == true) {
                temp.SubjectIsBlank = true;
                temp.mySubjectShort = "BN";
            } else {
                temp.mySubjectShort = GraphHelperUtil.cropURI(subject.toString(), myFullModel.getNsPrefixMap());
            }

            Property predicate = stmt.getPredicate();
            temp.myPredicate = predicate.toString();
            temp.myPredicateShort = GraphHelperUtil.cropURI(predicate.toString(), myFullModel.getNsPrefixMap());

            RDFNode myNode = stmt.getObject();
            temp.myObject = myNode.toString();
            if (myNode.isLiteral()) {
                temp.ObjectIsLiteral = true;
                temp.myObjectShort = GraphHelperUtil.cropLiteral(temp.myObject);
            } else if (myNode.asNode().isBlank() == true) {
                temp.myObjectShort = "BN";
                temp.ObjectIsBlank = true;
            } else {
                temp.myObjectShort = GraphHelperUtil.cropURI(myNode.toString(), myFullModel.getNsPrefixMap());
            }

            myStatements.add(temp);
        }
        return myStatements;
    }

    @Override
    public VisualiseWrapper CenterOn(String _center, NodeType _type) {
        VisualiseWrapper result;
        ArrayList<TripleStatement> myStatements;
        Queue<RDFNode> myQ = new LinkedList<>();
        myQ.add(getAsRDFNode(_type, _center));
        HashSet<RDFNode> processedNodes = new HashSet<>();
        Model subModel = ModelFactory.createDefaultModel();

        if (!myFullModel.containsResource(getAsResource(_type, _center))) {
            result = new VisualiseWrapper(null, VisualiseStatus.UnableToFindFocalPoint);
        } else {
            while (!myQ.isEmpty()
                    && size(subModel) < maxVertices) {
                RDFNode center = myQ.remove();
                CenterOnProcessNode(processedNodes, subModel, center, myQ);
            }
            myStatements = getStatements(subModel);
            result = new VisualiseWrapper(myStatements, VisualiseStatus.Success);
        }
        return result;
    }

    @Override
    public VisualiseWrapper CenterOn() {
        VisualiseWrapper result;
        ArrayList<TripleStatement> myStatements;
        Queue<RDFNode> myQ = new LinkedList<>();
        myQ.add(getAsRDFNode(this.myCenterType, this.myCurrentCenter));
        HashSet<RDFNode> processedNodes = new HashSet<>();
        Model subModel = ModelFactory.createDefaultModel();

        if (!myFullModel.containsResource(getAsResource(this.myCenterType, this.myCurrentCenter))) {
            result = new VisualiseWrapper(null, VisualiseStatus.UnableToFindFocalPoint);
        } else {
            while (!myQ.isEmpty()
                    && size(subModel) < maxVertices) {
                RDFNode center = myQ.remove();
                CenterOnProcessNode(processedNodes, subModel, center, myQ);
            }
            myStatements = getStatements(subModel);
            result = new VisualiseWrapper(myStatements, VisualiseStatus.Success);
        }
        return result;
    }

    private void CenterOnProcessNode(HashSet<RDFNode> _p, Model _m, RDFNode _c, Queue<RDFNode> _q) {

        StmtIterator stmts;
        // _c as sbj which cannot be literal
        if (!_c.isLiteral()) {
            stmts = myFullModel.listStatements(_c.asResource(), null, (RDFNode) null);
            while (stmts.hasNext() && size(_m) < maxVertices) {
                Statement stmt = stmts.next();

                if (!_m.contains(stmt)) {
                    _m.add(stmt);

                    if (!_p.contains(stmt.getObject())) {
                        _p.add(stmt.getObject());
                        _q.add(stmt.getObject());
                    }
                }
            }
        }

        // _c as obj
        stmts = myFullModel.listStatements(null, null, _c);
        while (stmts.hasNext() && size(_m) < maxVertices) {
            Statement stmt = stmts.next();

            if (!_m.contains(stmt)) {
                _m.add(stmt);

                if (!_p.contains(myFullModel.asRDFNode(stmt.getSubject().asNode()))) {
                    _p.add(myFullModel.asRDFNode(stmt.getSubject().asNode()));
                    _q.add(myFullModel.asRDFNode(stmt.getSubject().asNode()));
                }
            }
        }
    }

    /**
     * Returns basis for LocalGraph as a Jena Model
     *
     * @return Returns Jena Model
     */
    public Model getModel() {
        return this.myFullModel;
    }

    /**
     * Probes whether model is ready to be visualised. Always returns true for
     * LocalGraphs
     *
     * @return Always returns true
     */
    @Override
    public ReadyToVisualise ReadyToVisualise() {
        return ReadyToVisualise.Ready;
    }

    /**
     * Updates the sub-model with a new center
     *
     * @param _newCenter Center for new sub-model
     * @param _type Type of center
     * @throws QueryExceptionHTTP No exception thrown
     */
    @Override
    public void UpdateModel(String _newCenter, NodeType _type) throws QueryExceptionHTTP {
        this.setCenterVertex(_newCenter, _type);

        if (this.myCurrentCenter.length() > 0) {
            String constructStmt = String.format("CONSTRUCT { <%s> ?p ?o}\n"
                    + "WHERE {\n"
                    + "<%s> ?p ?o.\n"
                    + "}", this.myCurrentCenter, this.myCurrentCenter);

            if (this.mySubModel != null) {
                this.mySubModel.close();
                this.mySubModel = null;
            }

            this.mySubModel = this.performConstruct(constructStmt);
        } else if (this.myCurrentCenter.compareTo("") == 0) {
            if (this.mySubModel != null) {
                this.mySubModel.close();
                this.mySubModel = null;
            }
        }
    }

    public void UpdateModel2(String _newCenter, NodeType _type) throws QueryExceptionHTTP {
        this.setCenterVertex(_newCenter, _type);
        List<RDFNode> myList = new ArrayList<>();
        List<RDFNode> myFrontier = new ArrayList<>();
        RDFNode c = getAsRDFNode(_type, _newCenter);

        if (this.mySubModel != null) {
            this.mySubModel.close();
            this.mySubModel = null;
            this.mySubModel = ModelFactory.createDefaultModel();
        }

    }

    /**
     * Updates the sub-model with the already set center
     *
     * @throws QueryExceptionHTTP No exception thrown
     */
    @Override
    public void UpdateModel() throws QueryExceptionHTTP {

        if (this.myCurrentCenter.length() > 0) {
            String constructStmt = String.format("CONSTRUCT { <%s> ?p ?o}\n"
                    + "WHERE {\n"
                    + "<%s> ?p ?o.\n"
                    + "}", this.myCurrentCenter, this.myCurrentCenter);

            if (this.mySubModel != null) {
                this.mySubModel.close();
                this.mySubModel = null;
            }

            this.mySubModel = this.performConstruct(constructStmt);
        } else if (this.myCurrentCenter.compareTo("") == 0) {
            if (this.mySubModel != null) {
                this.mySubModel.close();
                this.mySubModel = null;
            }
        }
    }

    /**
     * Sets a new node as venter.
     *
     * @param _newCenter Node to become new center
     * @param _type Type of new center
     */
    @Override
    public void setCenterVertex(String _newCenter, NodeType _type) {
        this.myCurrentCenter = _newCenter;
        this.myCenterType = _type;

        // center vertex has been cleared, we therefore
        // remove subgraph
        if (this.myCurrentCenter.compareTo("") != 0) {
            if (this.mySubModel != null) {
                this.mySubModel.close();
                this.mySubModel = null;
            }
        }
    }

    /**
     * Returns the current model center
     *
     * @return Value of current center as a String
     */
    @Override
    public String getCenterVertex() {
        return this.myCurrentCenter;
    }

    /**
     *
     * @param _vertex
     * @param _newURI
     * @return Returns ResourceResult indicating result of method
     */
    @Override
    public ResourceResult UpdateVertexURI(String _vertex, String _newURI) {
        ResourceResult results = new ResourceResult();
        Resource oldVertex = getAsResource(NodeType.Vertex, _vertex);
        Resource newVertex = getAsResource(NodeType.Vertex, _newURI);

        if (!GraphHelperUtil.isValidURI(_newURI)) {
            results.actionReport = UpdateVertexURI.InvalidURI;
        } else if (myFullModel.containsResource(newVertex)) {
            results.actionReport = UpdateVertexURI.DuplicateURLNotAllowed;
        } else if (!myFullModel.containsResource(oldVertex)) {
            results.actionReport = UpdateVertexURI.VertexNotFound;
        } else {
            ResourceUtils.renameResource(oldVertex, _newURI);
            results.nodeValue = newVertex.toString();
            results.nodeValueShort = GraphHelperUtil.cropURI(newVertex.toString());
            results.actionReport = UpdateVertexURI.Success;
        }
        return results;
    }

    @Override
    public ResourceResult VertexToBlankNode(String _vertex) {
        ResourceResult result = new ResourceResult();
        Resource oldVertex = getAsResource(NodeType.Vertex, _vertex);

        if (myFullModel.containsResource(oldVertex)) {
            Resource newR = ResourceUtils.renameResource(oldVertex, null);
            result.nodeValue = newR.toString();
            result.actionReport = VertexToBlankNode.Success;
        } else {
            result.actionReport = VertexToBlankNode.VertexNotFound;
        }
        return result;
    }

    @Override
    public DeleteVertex DeleteVertex(String _node) {
        Resource vertex = getAsResource(NodeType.Vertex, _node);

        if (myFullModel.containsResource(vertex)) {
            // Removing where vertex is subject
            this.myFullModel.removeAll(vertex, null, null);

            // Removing where vertex is object
            this.myFullModel.removeAll(null, null, vertex);
            return DeleteVertex.Success;
        } else {
            return DeleteVertex.VertexNotFound;
        }
    }

    @Override
    public ResourceResult InsertVertex(String _value) {
        ResourceResult result = new ResourceResult();
        Resource vertex = getAsResource(NodeType.Vertex, _value);

        if (myFullModel.containsResource(vertex)) {

            result.actionReport = InsertVertex.DuplicateURLNotAllowed;
        } else if (!GraphHelperUtil.isValidURI(_value)) {
            result.actionReport = InsertVertex.InvalidURI;
        } else {
            Resource a = myFullModel.createResource(_value);
            result.nodeValue = a.toString();
            result.nodeValueShort = GraphHelperUtil.cropURI(a.toString());
            result.actionReport = InsertVertex.Success;
        }
        return result;
    }

    @Override
    public ResourceResult ResolveBlankNode(String _nodeID, String _newURI) {
        ResourceResult result = new ResourceResult();
        Resource oldVertex = getAsResource(NodeType.BlankNode, _nodeID);
        Resource newVertex = getAsResource(NodeType.Vertex, _newURI);

        if (myFullModel.containsResource(newVertex)) {
            result.actionReport = UpdateVertexURI.DuplicateURLNotAllowed;
        } else if (!GraphHelperUtil.isValidURI(_newURI)) {
            result.actionReport = UpdateVertexURI.InvalidURI;
        } else if (!myFullModel.containsResource(oldVertex)) {
            result.actionReport = UpdateVertexURI.VertexNotFound;
        } else {
            Resource newR = ResourceUtils.renameResource(oldVertex, _newURI);

            result.nodeValue = newR.toString();
            result.nodeValueShort = GraphHelperUtil.cropURI(newR.toString());
            result.actionReport = UpdateVertexURI.Success;
        }
        return result;
    }

    @Override
    public DeleteVertex DeleteBlankNode(String _nodeID) {
        Resource blankNode = getAsResource(NodeType.BlankNode, _nodeID);

        if (myFullModel.containsResource(blankNode)) {
            // Removing where vertex is subject
            this.myFullModel.removeAll(blankNode, null, null);

            // Removing where vertex is object
            this.myFullModel.removeAll(null, null, blankNode);
            return DeleteVertex.Success;
        } else {
            return DeleteVertex.VertexNotFound;
        }
    }

    @Override
    public ResourceResult InsertBlankNode() {
        ResourceResult result = new ResourceResult();
        Resource r = myFullModel.createResource();
        result.nodeValue = r.toString();
        result.actionReport = InsertVertex.Success;
        return result;
    }

    @Override
    public UpdateLiteralText UpdateLiteralText(String _literal, String _newText) {
        RDFNode oldLiteral = getAsRDFNode(NodeType.Literal, _literal);
        RDFNode newLiteral = getAsRDFNode(NodeType.Literal, _newText);

        if (myFullModel.contains(null, null, newLiteral)) {
            return UpdateLiteralText.DuplicatesNotAllowed;
        } else if (!myFullModel.contains(null, null, oldLiteral)) {
            return UpdateLiteralText.LiteralNotFound;
        } else {
            ResourceUtils.renameResource(oldLiteral.asResource(), _newText);
            return UpdateLiteralText.Success;
        }
    }

    @Override
    public DeleteLiteral DeleteLiteral(String _literal) {
        RDFNode literal = getAsRDFNode(NodeType.Literal, _literal);

        if (myFullModel.contains(null, null, literal)) {
            this.myFullModel.removeAll(null, null, literal);
            return DeleteLiteral.Success;
        } else {
            return DeleteLiteral.LiteralNotFound;
        }
    }

    @Override
    public InsertLiteral InsertLiteral(String _text) {
        RDFNode literal = getAsRDFNode(NodeType.Literal, _text);

        if (myFullModel.contains(null, null, literal)) {
            return InsertLiteral.DuplicatesNotAllowed;
        } else {
            myFullModel.createResource(_text);
            return InsertLiteral.Success;
        }
    }

    @Override
    public DeleteEdge DeleteEdge(String _model,
            String _edgeURI, String _source,
            NodeType _sourceType, String _target,
            NodeType _targetType) {

        Resource source = getAsResource(_sourceType, _source);
        RDFNode target = getAsRDFNode(_targetType, _target);
        Property pred = myFullModel.getProperty(_edgeURI);

        if (!myFullModel.containsResource(source)) {
            return DeleteEdge.SubjectNotFound;
        } else if (!myFullModel.containsResource(target)) {
            return DeleteEdge.ObjectNotFound;
        } else {
            myFullModel.remove(source, pred, target);
            return DeleteEdge.Success;
        }
    }

    @Override
    public ResourceResult ChangeEdgeURI(String _edgeURI, String _source, NodeType _sourceType, String _target, NodeType _targetType, String _newEdgeURI) {
        ResourceResult result = new ResourceResult();

        Resource source = getAsResource(_sourceType, _source);
        RDFNode target = getAsRDFNode(_targetType, _target);
        Property pred = myFullModel.getProperty(_edgeURI);

        if (!myFullModel.containsResource(source)) {
            result.actionReport = InsertEdge.SubjectNotFound;
        } else if (!myFullModel.containsResource(target)) {
            result.actionReport = InsertEdge.ObjectNotFound;
        } else {
            Property newEdge = myFullModel.getProperty(_newEdgeURI);

            // Removing triple
            myFullModel.remove(source, pred, target);

            // Adding new triple
            myFullModel.add(source, newEdge, target);

            result.nodeValue = newEdge.toString();
            result.nodeValueShort = GraphHelperUtil.cropURI(newEdge.toString());
            result.actionReport = ChangeEdge.Success;
        }
        return result;
    }

    @Override
    public ResourceResult InsertEdge(String _edgeURI, String _source, NodeType _sourceType, String _target, NodeType _targetType) {
        ResourceResult result = new ResourceResult();

        Resource source = getAsResource(_sourceType, _source);
        RDFNode target = getAsRDFNode(_targetType, _target);

        if (!myFullModel.containsResource(source)) {
            result.actionReport = InsertEdge.SubjectNotFound;
        } else if (!myFullModel.containsResource(target)) {
            result.actionReport = InsertEdge.ObjectNotFound;
        } else if (!GraphHelperUtil.isValidURI(_edgeURI)) {
            result.actionReport = InsertEdge.InvalidURI;
        } else if (myFullModel.contains(source, myFullModel.getProperty(_edgeURI), target)) {
            result.actionReport = InsertEdge.DuplicateNotAllowed;
        } else {
            Property pred = myFullModel.getProperty(_edgeURI);
            myFullModel.add(source, pred, target);
            result.nodeValue = pred.toString();
            result.nodeValueShort = GraphHelperUtil.cropURI(pred.toString());
        }
        return result;
    }

    private Resource getAsResource(NodeType _type, String _value) {
        return getAsRDFNode(_type, _value).asResource();
    }

    private RDFNode getAsRDFNode(NodeType _type, String _value) {
        return myFullModel.asRDFNode(getAsNode(_type, _value));
    }

    private Node getAsNode(NodeType _type, String _value) {
        Node result = null;
        switch (_type) {
            case BlankNode:
                result = NodeFactory.createAnon(new AnonId(_value));
                break;
            case Edge:
                result = NodeFactory.createURI(_value);
                break;
            case Literal:
                result = NodeFactory.createLiteral(_value);
                break;
            case Vertex:
                result = NodeFactory.createURI(_value);
                break;
        }
        return result;
    }

    @Override
    public NodeType getNodeType(String _value) {
        Resource node = myFullModel.getResource(_value);
        if (node.isAnon()) {
            return NodeType.Literal;
        } else if (node.isLiteral()) {
            return NodeType.Literal;
        } else if (node.isURIResource()) {
            return NodeType.Vertex;
        } else {
            return null;
        }
    }

    /**
     * Describes a node based on the Concise Bounded Description method Source:
     * http://www.w3.org/Submission/CBD/
     *
     * @param _vertex
     * @return Returns ArrayList of triples
     */
    @Override
    public ArrayList<TripleStatement> CBD(String _vertex) {
        Resource c = getAsResource(myCenterType.Vertex, _vertex);
        ArrayList<TripleStatement> result = new ArrayList<>();

        if (!myFullModel.containsResource(c)) {

        } else {
            Model tempModel = ModelFactory.createDefaultModel();
            Queue<Resource> frontier = new LinkedList<>();
            frontier.add(c);
            List<Statement> stmts;
            Resource center;

            while (frontier.size() > 0
                    && myFullModel.size() < maxVertices) {
                center = frontier.remove();
                stmts = myFullModel.listStatements(center, null,
                        (RDFNode) null).toList();
                for (Statement s : stmts) {
                    tempModel.add(s);
                    RDFNode obj = s.getObject();
                    if (obj.isAnon() == true) {
                        frontier.add(obj.asResource());
                    }
                }
            }
            result = getTriples(tempModel);
        }
        return result;
    }

    /**
     * Describes a node based on the Symmetric Concise Bounded Description
     * method Source: http://www.w3.org/Submission/CBD/
     *
     * @param _literal Node to be described
     * @return List of triples based on description
     */
    @Override
    public ArrayList<TripleStatement> SCBD(String _literal) {
        RDFNode c = getAsRDFNode(NodeType.Literal, _literal);
        ArrayList<TripleStatement> result = new ArrayList<>();

        if (!myFullModel.containsResource(c)) {

        } else {
            Model tempModel = ModelFactory.createDefaultModel();
            Queue<Resource> frontier = new LinkedList<>();
            List<Resource> visitedNodes = new LinkedList<>();
            List<Statement> stmts;
            Resource center;

            // Step 1 - c as object, add all blank subjects to frontier
            stmts = myFullModel.listStatements(null, null, c).toList();
            for (Statement s : stmts) {
                tempModel.add(s);
                Resource sbj = s.getSubject();
                if (sbj.isAnon()) {
                    frontier.add(sbj);
                }
            }

            // Step 2 - Recursively search out new blank nodes and their 
            // properties.
            while (frontier.size() > 0
                    && myFullModel.size() < maxVertices) {
                center = frontier.remove();
                visitedNodes.add(center);
                stmts = myFullModel.listStatements(
                        center,
                        null,
                        (RDFNode) null).toList();

                for (Statement s : stmts) {
                    tempModel.add(s);
                    RDFNode obj = s.getObject();
                    if (obj.isAnon() == true) {
                        frontier.add(obj.asResource());
                    }
                }
            }
            result = getTriples(tempModel);
        }
        return result;
    }

    /**
     * Iterates over a model and returns an ArrayList of triples for use during
     * visualisation
     *
     * @param _literal Model to be iterated over
     * @return Returns an empty list for an empty model, otherwise returns
     * filled ArrayList
     */
    private ArrayList<TripleStatement> getTriples(Model _m) {
        ArrayList<TripleStatement> result = new ArrayList<>();
        TripleStatement tempStmt;
        StmtIterator itr = _m.listStatements();

        while (itr.hasNext()) {
            tempStmt = new TripleStatement();
            Statement stmt = itr.nextStatement();

            Resource subject = stmt.getSubject();
            tempStmt.mySubject = subject.toString();
            if (subject.asNode().isBlank() == true) {
                tempStmt.SubjectIsBlank = true;
                tempStmt.mySubjectShort = "BN";
            } else {
                tempStmt.mySubjectShort = GraphHelperUtil.cropURI(subject.toString(), myFullModel.getNsPrefixMap());
            }

            Property predicate = stmt.getPredicate();
            tempStmt.myPredicate = predicate.toString();
            tempStmt.myPredicateShort = GraphHelperUtil.cropURI(predicate.toString(), myFullModel.getNsPrefixMap());

            RDFNode myNode = stmt.getObject();
            tempStmt.myObject = myNode.toString();
            if (myNode.isLiteral()) {
                tempStmt.ObjectIsLiteral = true;
                tempStmt.myObjectShort = GraphHelperUtil.cropLiteral(tempStmt.myObject);
            } else if (myNode.asNode().isBlank() == true) {
                tempStmt.myObjectShort = "BN";
                tempStmt.ObjectIsBlank = true;
            } else {
                tempStmt.myObjectShort = GraphHelperUtil.cropURI(myNode.toString(), myFullModel.getNsPrefixMap());
            }
            result.add(tempStmt);
        }
        return result;
    }

    @Override
    public TripleStatementWrapper InterestingSearch(String _bagOfWords,
            int _maxNodes,
            int _maxDist, String _initNode, NodeType _initType,
            double _beta, String[] _filters, InterestingGraphThread _report,
            String _lang) 
    {

        // @todo implement filters
        Resource initNode = getAsResource(_initType, _initNode);
        TripleStatementWrapper result = new TripleStatementWrapper();
        PriorityQueue<RDFTreeNode> myFrontier = new PriorityQueue<>();
        WordScorer myScorer = new WordScorer(_bagOfWords, _beta);
        RDFTree myTree;
        RDFTreeNode root, current;
        HashSet<RDFNode> processedNodes = new HashSet<>();

        if (!myFullModel.containsResource(initNode)) {
            result.actionReport = InterestingResult.InitialNodeNotFound;
        } else {
            // Setting up initial conditions...
            root = new RDFTreeNode(myFullModel.asRDFNode(initNode.asNode()));
            root.setMyMatchScore(1000);   // We always want root to remain
            myTree = new RDFTree(root);
            myFrontier.add(root);
            processedNodes.add(initNode);

            while (myTree.getDepth() < _maxDist
                    && myTree.getCount() < _maxNodes
                    && myFrontier.size() > 0) {
                current = myFrontier.remove();
                processedNodes.add(current.getMyValue());
                processNeighbours(current, myFrontier, myTree, processedNodes, myScorer);
            }

            // Cleaning tree
            RDFTreeMatchScoreFilter myFilter = new RDFTreeMatchScoreFilter(myTree);
            myFilter.filterOriginal();

            try {
                PrintWriter writer = new PrintWriter("output.txt");
                myTree.printNodes(writer);
                writer.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(LocalGraph.class.getName()).log(Level.SEVERE, null, ex);
            }
            result.myTriples = myTree.getInterestingTriples(myFullModel);
            result.actionReport = InterestingResult.Success;
        }
        return result;
    }

    private void processNeighbours(RDFTreeNode _c,
            PriorityQueue<RDFTreeNode> _frontier,
            RDFTree _tree,
            HashSet<RDFNode> _processed,
            WordScorer _scorer) {

        RDFNode curr = _c.getMyValue();
        Statement stmt;

        // Finding all stmts where _c is sbj
        StmtIterator stmts = myFullModel.listStatements(curr.asResource(), null, (RDFNode) null);

        while (stmts.hasNext()) {
            stmt = stmts.next();
            RDFNode obj = stmt.getObject();

            if (!_processed.contains(obj) && !obj.isLiteral()) {
                RDFTreeNode temp = new RDFTreeNode(obj);
                double score = scoreNode(obj, _scorer);
                temp.setMyMatchScore(score);

                // Adding to tree
                if (!_tree.contains(temp)) {
                    _tree.addNode(_c, temp);
                }

                // Adding to frontier
                if (!_frontier.contains(temp)) {
                    _frontier.add(temp);
                }
            }
        }

        stmts = myFullModel.listStatements(null, null, curr);

        while (stmts.hasNext()) {
            stmt = stmts.next();
            RDFNode sbj = stmt.getSubject();

            if (!_processed.contains(sbj) && !sbj.isLiteral()) {
                RDFTreeNode temp = new RDFTreeNode(sbj);
                double score = scoreNode(sbj, _scorer);

                temp.setMyMatchScore(score);

                // Adding to tree
                if (!_tree.contains(temp)) {
                    _tree.addNode(_c, temp);
                }

                // Adding to frontier
                if (!_frontier.contains(temp)) {
                    _frontier.add(temp);
                }
            }
        }
    }

    private double scoreNode(RDFNode _node, WordScorer _scorer) {
        ArrayList<String> docs = new ArrayList<>();

        StmtIterator neighbours = myFullModel.listStatements(_node.asResource(), null, (RDFNode) null);
        while (neighbours.hasNext()) {
            Statement s = neighbours.next();
            if (s.getObject().isLiteral()) {
                docs.add(s.getObject().asLiteral().getString());
            }
        }

        if (docs.isEmpty()) {
            return 0.0;
        } else {
            return _scorer.ScoreDocuments(docs);
        }
    }

    @Override
    public InterestingLiterals getLiterals(String _center, NodeType _centerType) {
        InterestingLiterals result = new InterestingLiterals();
        Resource c = getAsResource(_centerType, _center);
        StmtIterator stmts = myFullModel.listStatements(c, null, (RDFNode) null);

        while (stmts.hasNext()) {
            Statement stmt = stmts.next();

            if (stmt.getObject().isLiteral()) {
                LiteralTriple triple = new LiteralTriple(
                        _center,
                        stmt.getPredicate().toString(),
                        GraphHelperUtil.cropURI(stmt.getPredicate().toString(), myFullModel.getNsPrefixMap()),
                        stmt.getObject().toString(),
                        GraphHelperUtil.cropLiteral(stmt.getObject().toString()));
                result.myLiteral.add(triple);
            }
        }
        result.actionReport = InterestingResult.Success;
        return result;
    }

    @Override
    public int getMaxVertices() {
        return this.maxVertices;
    }

    @Override
    public void setMaxVertices(int _max) {
        this.maxVertices = _max;
    }

    /**
     * Returns the number of entities for the given model
     *
     * @param _model Model to explore
     * @return Number of entities or -1 if error
     */
    private int size(Model _model) {
        String countQuery = "SELECT (COUNT(*) AS ?count) {\n"
                + "   SELECT DISTINCT ?s ?o {\n"
                + "     ?s ?p ?o.\n"
                + "   }\n"
                + "}";
        try {
            Query query = QueryFactory.create(countQuery);

            QueryExecution qExe = QueryExecutionFactory.create(query, _model);
            ResultSet results = qExe.execSelect();

            QuerySolution solution = results.next();
            return solution.get("count").asLiteral().getInt();
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    /**
     * Gets the number of entities in the full model.
     *
     * @return Returns the number of entities or -1
     */
    private int size() {
        String countQuery = "SELECT (COUNT(*) AS ?count) {\n"
                + "   SELECT DISTINCT ?s ?o {\n"
                + "     ?s ?p ?o.\n"
                + "   }\n"
                + "}";
        try {
            Query query = QueryFactory.create(countQuery);

            QueryExecution qExe = QueryExecutionFactory.create(query, this.myFullModel);
            ResultSet results = qExe.execSelect();

            QuerySolution solution = results.next();
            numOfEntities = solution.get("count").asLiteral().getInt();
            return numOfEntities;

        } catch (NumberFormatException e) {
            return -1;
        }
    }

    @Override
    public DisambiguityResults DisambiguitySearch(String _searchTerm, String _lang) {
        DisambiguityResults result;

        String q;
        if (_lang.length() > 0) {
            q = String.format(
                    "SELECT ?s ?l (count(?s) AS ?count) WHERE {\n"
                    + "?someobj ?p ?s .\n"
                    + "?s <http://www.w3.org/2000/01/rdf-schema#label> ?l .\n"
                    + "?l <bif:contains> '\"%s\"' .\n"
                    + "FILTER (!regex(str(?s), '^http://dbpedia.org/resource/Category:')).\n"
                    + "FILTER (!regex(str(?s), '^http://dbpedia.org/resource/List')).\n"
                    + "FILTER (!regex(str(?s), '^http://sw.opencyc.org/')).\n"
                    + "FILTER (lang(?l) = '%s').\n"
                    + "FILTER (!isLiteral(?someobj)).\n"
                    + "} GROUP BY ?s ?l ORDER BY DESC(?count) LIMIT 20", _searchTerm, _lang);
        } else {
            q = String.format(
                    "SELECT ?s ?l (count(?s) AS ?count) WHERE {\n"
                    + "?someobj ?p ?s .\n"
                    + "?s <http://www.w3.org/2000/01/rdf-schema#label> ?l .\n"
                    + "?l <bif:contains> '\"%s\"' .\n"
                    + "FILTER (!regex(str(?s), '^http://dbpedia.org/resource/Category:')).\n"
                    + "FILTER (!regex(str(?s), '^http://dbpedia.org/resource/List')).\n"
                    + "FILTER (!regex(str(?s), '^http://sw.opencyc.org/')).\n"
                    + "FILTER (lang(?l) = 'en').\n"
                    + "FILTER (!isLiteral(?someobj)).\n"
                    + "} GROUP BY ?s ?l ORDER BY DESC(?count) LIMIT 20", _searchTerm);
        }

        Query query = QueryFactory.create(q);
        QueryExecution qExe = QueryExecutionFactory.create(query, this.myFullModel);
        ResultSet results = qExe.execSelect();

        ArrayList<DisambiguityResult> temp = new ArrayList<>();
        while (results.hasNext()) {
            QuerySolution s = results.next();
            temp.add(new DisambiguityResult(s.get("s").toString(), s.get("l").toString(), s.get("count").toString()));
        }

        result = new DisambiguityResults(InterestingResult.Success, temp);
        return result;
    }
    
    @Override
    public ArrayList<InterestingPath> FetchPaths(String _query) throws Exception {
        return null;
    }

    @Override
    public InterestingPath[] InterestingPaths(String[] _NSFilters, String _source, String _target, String _searchTerms, String _lang, int _minPath, int _maxPath, int _maxNumOfPaths, InterestingPathsThread _report) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * We have no timeouts for local files
     * @param _timeout 
     */
    @Override
    public void setTimeout(int _timeout) {}

    @Override
    public int getTimeout() {
        return -1;
    }

    @Override
    public String getEndpointURL() {
        return "";
    }

    @Override
    public void setEndpointURL(String _newURI) {
        
    }
}
