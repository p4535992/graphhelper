/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.InterestingSearch;

import DataSources.GenericHTTPEndPoint;
import GUI.Threads.InterestingGraphThread;
import GUI.Threads.ScoreTreeNodesThread;
import Graph.Algorithm.PBFS.TDB;
import Graph.Enumerator.InterestingResult;
import Graph.Implementations.GraphHelperUtil;
import Graph.Implementations.SPARQLFactory;
import Graph.Implementations.SPARQL_ResultSet;
import Graph.Implementations.TripleStatementWrapper;
import Graph.Interfaces.DataSourceInterface;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import java.util.Collection;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Vector;

public class InterestingGraphSearch {

    private final DataSourceInterface myMaster;
    private final String myBagOfWords, myInitialNode, myLang;
    private final int MyMaxNodes, myMaxDist;
    private int numOfInteresting = 0;
    private final double myBeta;
    private final String[] myFilters;
    private final InterestingGraphThread myReport;

    private RDFTree myTree;
    private final WordScorer myScorer;
    private final PriorityQueue<RDFTreeNode> myFrontier = new PriorityQueue<>();
    private final TripleStatementWrapper result = new TripleStatementWrapper();
    private final Model myModel;
    private final HashSet<RDFNode> processedNodes = new HashSet<>();
    private Set<RDFTreeNode> myNewNodes = new HashSet<>();

    // <editor-fold defaultstate="collapsed" desc="Constructor">
    /**
     *
     * @param _master Source to execute SPARQL against
     * @param _m Model to perform search on
     * @param _bag String containing bag-of-words search terms
     * @param _init Initial node to search from
     * @param _lang Language restriction on literals. If null, no restriction
     * @param _maxNode Maximum number of interesting nodes returned
     * @param _maxDist Maximum distance from _init
     * @param _beta Score weight
     * @param _filters Namespace filters
     * @param _reporter Thread to report progress to
     */
    public InterestingGraphSearch(DataSourceInterface _master, Model _m,
            String _bag, String _init, String _lang,
            int _maxNode, int _maxDist,
            double _beta, String[] _filters, InterestingGraphThread _reporter) {
        myMaster = _master;
        myBagOfWords = _bag;
        myInitialNode = _init;
        myLang = _lang;
        MyMaxNodes = _maxNode;
        myMaxDist = _maxDist;
        myBeta = _beta;
        myReport = _reporter;
        myModel = _m;

        // Setting up scorer
        myScorer = new WordScorer(myBagOfWords, myBeta);

        myFilters = _filters;

        // All filters to lower
        for (int i = 0; i < myFilters.length; i++) {
            myFilters[i] = myFilters[i].toLowerCase();
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Public Method">
    /**
     * Executes interesting search
     *
     * @return Returns TripleStatementWrapper with data or error type
     */
    public TripleStatementWrapper InterestingSearch() {

        Resource initNode = null;
        try {
            initNode = myModel.getResource(myInitialNode);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        RDFTreeNode root;

        if (!myModel.containsResource(initNode)) {
            result.actionReport = InterestingResult.InitialNodeNotFound;
        } else {
            // Setting up initial conditions...
            root = new RDFTreeNode(myModel.asRDFNode(initNode.asNode()));
            root.setMyMatchScore(1000);   // We always want root to remain
            myTree = new RDFTree(root);
            myFrontier.add(root);

            // Building tree...
            BuildTree();

            // Cleaning
            RecursiveInterestingPruning myFilter = new RecursiveInterestingPruning(myTree, MyMaxNodes, numOfInteresting);
            myFilter.filter();

            result.myTriples = myTree.getInterestingTriples(myModel);
            result.actionReport = InterestingResult.Success;
        }

        return result;
    }
    // </editor-fold>

    /**
     * Processes one node from the frontier
     */
    @SuppressWarnings("unchecked")
    private void processNeighbour() {
        // Getting node from frontier
        RDFTreeNode current = myFrontier.remove();

        // Adding node to blacklist
        processedNodes.add(current.getMyValue());

        // No need to process if the resulting node is too far out
        if (current.getMyDistance() + 1 <= this.myMaxDist) {
            // New temp list
            myNewNodes = new HashSet<>();

            // Processing where current is subject
            CurrentIsSubject(current.getMyValue().asResource());

            // Processing where current is object
            CurrentIsObject(current.getMyValue());

            //Scoring nodes
            ScoreNodes(current);
        }
    }

    private void ScoreNodesMT(Multimap<String, String> _literals, RDFTreeNode _parent) {
        // Divide myNewNodes into subsets
        int threads = Runtime.getRuntime().availableProcessors();
        Set<RDFTreeNode>[] mySets = new HashSet[threads];

        for (int i = 0; i < mySets.length; i++) {
            mySets[i] = new HashSet<>();
        }
        int ptr = 0;
        for (RDFTreeNode node : myNewNodes) {
            mySets[ptr].add(node);
            ptr++;
            if (ptr >= threads) {
                ptr = 0;
            }
        }
        // Create threads
        ScoreTreeNodesThread[] myThreads = new ScoreTreeNodesThread[threads];
        for (int i = 0; i < myThreads.length; i++) {
            myThreads[i] = new ScoreTreeNodesThread(_literals, mySets[i], myScorer);
            myThreads[i].run();
        }
        // Wait for threads to finish
        try {
            for (ScoreTreeNodesThread thread : myThreads) {
                thread.join();
            }
        } catch (Exception e) {
        }
        
        
        // Add to tree
        for (RDFTreeNode tn : myNewNodes) {
            
            if(tn.getMyMatchScore() > 0.0) {
                this.numOfInteresting++;
            }
            
            // Adding node to frontier
            if (!myFrontier.contains(tn)) {
                myFrontier.add(tn);
            }

            // Adding node to tree
            if (!myTree.contains(tn)) {
                myTree.addNode(_parent, tn);
            }
        }

    }

    /**
     * Extracts literals for all URIs and scores nodes based on them
     */
    private void ScoreNodesST(Multimap<String, String> _literals, RDFTreeNode _parent) {

        // Scoring nodes and adding them to the tree
        for (RDFTreeNode tn : myNewNodes) {
            Collection<String> lits = _literals.get(tn.getNodeURI());
            double score = myScorer.ScoreDocuments(lits);

            // We keep track of all nodes with positive scores
            if (score > 0.0) {
                numOfInteresting++;
            }

            // Setting node scores
            tn.setMyMatchScore(score);

            // Adding node to frontier
            if (!myFrontier.contains(tn)) {
                myFrontier.add(tn);
            }

            // Adding node to tree
            if (!myTree.contains(tn)) {

                myTree.addNode(_parent, tn);
            }
        }
    }

    private void ScoreNodes(RDFTreeNode _parent) {

        String[] distinctNodes = GraphHelperUtil.DistinctURI(myNewNodes);
        Multimap<String, String> literals = HashMultimap.create();
        String[] literalQueries = SPARQLFactory.GetMultipleLiterals(distinctNodes, myLang);

        // Getting literals
        for (String literalQuerie : literalQueries) {
            SPARQL_ResultSet results = myMaster.performSelect(literalQuerie);
            if (results.validSet) {
                for (Vector<String> v1 : results.myData) {

                    if (v1.size() == 2) {
                        String sbj = v1.get(0);
                        String lit = v1.get(1);
                        literals.put(sbj, lit);
                    }
                }
            }
        }

        int threads = Runtime.getRuntime().availableProcessors();
        if (myNewNodes.size() * 2 > threads) {
            ScoreNodesMT(literals, _parent);
        } else {
            ScoreNodesST(literals, _parent);
        }
    }

    /**
     * Processes the frontier
     */
    private void BuildTree() {
        while (!myFrontier.isEmpty() && this.numOfInteresting < MyMaxNodes) {
            processNeighbour();
        }
    }

    /**
     * Tests whether _pred or _entity begins with the filtered Namespaces
     *
     * @param _pred Predicate to be tested
     * @param _entity Subject or object to be tested
     * @return True iff _pred or _entity starts with any of the filters
     */
    private boolean inFilters(String _pred, String _entity) {

        String p = _pred.toLowerCase();
        for (String s : this.myFilters) {
            if (p.startsWith(s)) {
                return true;
            }
        }

        String e = _entity.toLowerCase();
        for (String s : myFilters) {
            if (e.startsWith(s)) {
                return true;
            }
        }

        return false;
    }

    private void CurrentIsSubject(Resource _currRes) {
        Statement stmt;
        StmtIterator stmts;
        // Finding all stmts where current is subject
        stmts = myModel.listStatements(_currRes, null, (RDFNode) null);

        while (stmts.hasNext()) {
            stmt = stmts.next();
            RDFNode obj = stmt.getObject();

            // Only process if predicate and object are not in filters
            // and node hasn't been visited before
            if (!processedNodes.contains(obj)
                    && !obj.isLiteral()
                    && !inFilters(
                            stmt.getPredicate().getURI(),
                            obj.asResource().getURI())) {
                RDFTreeNode temp = new RDFTreeNode(obj);

                // Adding to temp list
                myNewNodes.add(temp);
            }
        }
    }

    private void CurrentIsObject(RDFNode _currRDF) {
        Statement stmt;
        StmtIterator stmts;

        // Getting all triples where current is the object
        stmts = myModel.listStatements(null, null, _currRDF);

        while (stmts.hasNext()) {
            stmt = stmts.next();
            RDFNode sbj = stmt.getSubject();

            // Only process if predicate and subject are not in filters
            // and node hasn't been visited before
            if (!processedNodes.contains(sbj)
                    && !sbj.isLiteral()
                    && !inFilters(
                            stmt.getPredicate().getURI(),
                            sbj.asResource().getURI())) {
                RDFTreeNode temp = new RDFTreeNode(sbj);

                // Adding to tree
                myNewNodes.add(temp);
            }
        }
    }
}
