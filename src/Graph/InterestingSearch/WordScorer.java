/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.InterestingSearch;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.LinkedList;

public class WordScorer {

    private final Properties myBagOfWordsProperties = new Properties();
    private StanfordCoreNLP myPipes;
    private final String mySearchString;
    private final List<String> myLemmas = new LinkedList<>();
    private final double betaValue;

    /**
     * Preprocesses a bag of words to be used for scoring.
     *
     * @param _mySearchString String to be parsed
     * @param _betaValue Weight for search terms
     */
    public WordScorer(String _mySearchString, double _betaValue) {
        mySearchString = _mySearchString;
        betaValue = _betaValue;

        // Processing the bag of words into lemmas
        myBagOfWordsProperties.put("annotators", "tokenize, ssplit, pos, lemma");
        myPipes = new StanfordCoreNLP(myBagOfWordsProperties);
        Annotation doc = new Annotation(mySearchString);
        myPipes.annotate(doc);

        // Looping through words
        for (CoreMap sentence : doc.get(SentencesAnnotation.class)) {
            for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
                String lemma = token.get(LemmaAnnotation.class);
                if (!myLemmas.contains(lemma.toLowerCase())) {
                    myLemmas.add(lemma.toLowerCase());
                }
            }
        }
    }

        /**
     * Generates a score for _docs based on beta *
     * (log(1+hits)/log(1+TotalLemmas))
     *
     * @param _docs Documents to be scored
     * @return Returns score calculated for document
     */
    public double ScoreDocumentsWithoutBeta(Collection<String> _docs) {
        int score = 0;
        int lemmaSpace = myLemmas.size();
        List<String> tempLemmas = new LinkedList<>(myLemmas);

        for (String d : _docs) {
            score += ScoreDocument(d, tempLemmas);
        }
        return (Math.log((double) (1 + score))
                / Math.log((double) (1 + lemmaSpace)));
    }
    /**
     * Generates a score for _docs based on beta *
     * (log(1+hits)/log(1+TotalLemmas))
     *
     * @param _docs Documents to be scored
     * @return Returns score calculated for document
     */
    public double ScoreDocuments(Collection<String> _docs) {
        int score = 0;
        int lemmaSpace = myLemmas.size();
        List<String> tempLemmas = new LinkedList<>(myLemmas);

        for (String d : _docs) {
            score += ScoreDocument(d, tempLemmas);
        }
        return betaValue
                * (Math.log((double) (1 + score))
                / Math.log((double) (1 + lemmaSpace)));
    }

    /**
     * Generates a score for _docs based on beta *
     * (log(1+hits)/log(1+TotalLemmas))
     *
     * @param _docs Documents to be scored
     * @return Returns score calculated for document
     */
    public double ScoreDocuments(List<String> _docs) {
        int score = 0;
        int lemmaSpace = myLemmas.size();
        List<String> tempLemmas = new LinkedList<>(myLemmas);

        for (String d : _docs) {
            score += ScoreDocument(d, tempLemmas);
        }
        return betaValue
                * (Math.log((double) (1 + score))
                / Math.log((double) (1 + lemmaSpace)));
    }

    /**
     *
     * @param _doc
     * @param _bagOfLemmas
     * @return Returns score calculated for document
     */
    private int ScoreDocument(String _doc, List<String> _bagOfLemmas) {
        int score = 0;
        Annotation doc = new Annotation(_doc);
        myPipes.annotate(doc);

        for (CoreMap sentence : doc.get(SentencesAnnotation.class)) {
            for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
                String lemma = token.get(LemmaAnnotation.class);
                if (Score(lemma.toLowerCase(), _bagOfLemmas) == true) {
                    score++;
                }
            }
        }
        return score;
    }

    /**
     * Scores _doc based on the bag of words provided to the constructor. The
     * maximum score is equal to the size of the bag of words.
     *
     * @param _doc Document to be scored.
     * @return Zero if no matches, up to size of bag of words.
     */
    public int ScoreDocument(String _doc) {
        int score = 0;
        LinkedList<String> tempLemmas = new LinkedList<>(myLemmas);
        Annotation doc = new Annotation(_doc);
        myPipes.annotate(doc);

        for (CoreMap sentence : doc.get(SentencesAnnotation.class)) {
            for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
                String lemma = token.get(LemmaAnnotation.class);
                if (Score(lemma.toLowerCase(), tempLemmas) == true) {
                    score++;
                }
            }
        }
        return score;
    }

    /**
     * Checks if _lemma is in _bagOfLemmas, and if true it removes _lemma from
     * the list.
     *
     * @param _lemma Weird to be searched for in _bagOfLemmas
     * @param _bagOfLemmas List to be searched
     * @return True iff _bagOfLemmas contains _lemma
     */
    private boolean Score(String _lemma, List<String> _bagOfLemmas) {
        if (_bagOfLemmas.contains(_lemma)) {
            _bagOfLemmas.remove(_lemma);
            return true;
        } else {
            return false;
        }
    }
}
