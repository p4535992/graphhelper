/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.InterestingSearch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class RecursiveInterestingPruning {

    private final RDFTree myTree;
    private final int maxInter;
    private int totalInter = 0;

    public RecursiveInterestingPruning(RDFTree _tree, int _maxInteresting, int _totalInteresting) {
        myTree = _tree;
        maxInter = _maxInteresting;

        for (RDFTreeNode tn : myTree.getNodes()) {
            if (tn.getMyMatchScore() > 0.0) {
                totalInter++;
            }
        }
    }

    public void filter() {
        pruneUninterestingLeaves();
        while (maxInter < totalInter) {
            pruneUninterestingLeaves();
            pruneInterestingLeaves(myTree.getRoot());
        }
    }

    private boolean pruneUninteresting(RDFTreeNode _node) {
        if (_node.getMyChildren() == null) {
            return (_node.getMyMatchScore() > 0);
        } else if (_node.getMyChildren().isEmpty()) {
            return (_node.getMyMatchScore() > 0);
        } // Node has children, check each child
        // and remove the ones without match score
        else {
            List<RDFTreeNode> nodeChildren = _node.getMyChildren();
            for (int i = nodeChildren.size() - 1; i >= 0; i--) {
                if (pruneUninteresting(nodeChildren.get(i)) == false) {
                    myTree.removeNode(nodeChildren.remove(i));
                }
            }
            // We don't have any children left, so return own score
            if (nodeChildren.isEmpty()) {
                return (_node.getMyMatchScore() > 0);
            } // We do have children with scores left, so return true
            else {
                return true;
            }
        }
    }

    private void pruneUninterestingLeaves() {
        List<RDFTreeNode> list = new ArrayList<>();

        for (RDFTreeNode tn : myTree.getNodes()) {
            if (!tn.hasChildren() && tn.getMyMatchScore() == 0.0) {
                list.add(tn);
            }
        }
        
        for(RDFTreeNode tn : list) {
            myTree.removeNode(tn);
        }
    }

    private void pruneInterestingLeaves(RDFTreeNode root) {
        try {
            PriorityQueue<RDFTreeNode> myLeafNodes = new PriorityQueue<>(50, Collections.reverseOrder());

            for (RDFTreeNode tn : myTree.getNodes()) {
                if (tn.getMyChildren() == null || tn.getMyChildren().isEmpty()) {
                    myLeafNodes.add(tn);
                }
            }

            while (!myLeafNodes.isEmpty() && maxInter < totalInter) {
                myTree.removeNode(myLeafNodes.remove());
                totalInter--;
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }

    }
}
