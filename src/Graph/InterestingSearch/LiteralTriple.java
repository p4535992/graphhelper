/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.InterestingSearch;

public class LiteralTriple {

    public String sbj;
    public String pred;
    public String predShort;
    public String lit;
    public String litShort;

    public LiteralTriple(
            String _sbj, 
            String _pred,
            String _predShort,
            String _lit, 
            String _litShort) {
        
        sbj = _sbj;
        pred = _pred;
        predShort = _predShort;
        lit = _lit;
        litShort = _litShort;
    }
}
