/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Implementations;

import Graph.InterestingSearch.InterestingLiterals;
import DataSources.GenericHTTPEndPoint;
import DataSources.LocalGraph;
import GUI.Threads.InterestingGraphThread;
import GUI.Threads.InterestingPathsThread;
import Graph.Enumerator.ChangeEdge;
import Graph.Enumerator.DeleteEdge;

import Graph.Enumerator.DeleteModel;
import Graph.Enumerator.ExportModel;
import Graph.Enumerator.FileFormat;
import Graph.Enumerator.InsertEdge;
import Graph.Interfaces.DataRepositoryInterface;
import Graph.Enumerator.ImportModel;

import Graph.Enumerator.InterestingResult;
import Graph.Enumerator.DeleteLiteral;
import Graph.Enumerator.InsertLiteral;
import Graph.Enumerator.UpdateLiteralText;
import Graph.Enumerator.NodeType;
import Graph.Enumerator.ReadyToVisualise;
import Graph.Enumerator.RunAskResult;
import Graph.Enumerator.SPARQLExpressionType;
import Graph.Enumerator.DeleteVertex;
import Graph.Enumerator.InsertVertex;
import Graph.Enumerator.UpdateVertexURI;
import Graph.Enumerator.VertexToBlankNode;
import Graph.Enumerator.VisualiseStatus;
import Graph.Interfaces.DataSourceInterface;
import Graph.InterestingPaths.DisambiguityResults;
import Graph.InterestingPaths.InterestingPath;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP;
import com.hp.hpl.jena.util.FileManager;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataStorage implements DataRepositoryInterface {

    private final HashMap<String, DataSourceInterface> myModels;

    public DataStorage() {
        this.myModels = new HashMap<>();
    }

    @Override
    public DeleteVertex DeleteVertex(String _model, String _node) {
        if (myModels.containsKey(_model)) {
            return myModels.get(_model).DeleteVertex(_node);
        } else {
            return DeleteVertex.ModelNotFound;
        }
    }

    @Override
    public ResourceResult InsertVertex(String _model, String _value) {

        ResourceResult result;
        if (myModels.containsKey(_model)) {
            result = myModels.get(_model).InsertVertex(_value);
        } else {
            result = new ResourceResult();
            result.actionReport = InsertVertex.ModelNotFound;
        }
        return result;
    }

    @Override
    public String[] GetModelNames() {
        String[] result = null;
        if (this.myModels.size() > 0) {
            Set<String> myKeys = this.myModels.keySet();
            result = new String[myKeys.size()];
            result = myKeys.toArray(result);
        }

        return result;
    }

    @Override
    public ExportModel ExportModel(String _model, String _path, FileFormat _format) {
        FileFormat format = _format;
        String path = _path;

        if (myModels.containsKey(_model) == false) {
            return ExportModel.ModelNotFound;
        } else {
            if (myModels.get(_model) instanceof LocalGraph) {
                Model model = ((LocalGraph) myModels.get(_model)).getModel();
                try {
                    FileOutputStream writer = new FileOutputStream(path);
                    switch (format) {
                        case RDF:
                            model.write(writer, "RDF/XML");
                            break;
                        case N3:
                            model.write(writer, "N3");
                            break;
                        case TURTLE:
                            model.write(writer, "TTL");
                            break;
                        case NTRIPLE:
                            model.write(writer, "NT");
                            break;
                        default:
                            model.write(writer, "");
                            break;
                    }

                    writer.close();
                } catch (Exception e) {
                    System.out.print(e.toString());
                    return ExportModel.FileParseError;
                }
            }
        }
        return ExportModel.Success;
    }

    @Override
    public DeleteModel DeleteModel(String _model) {
        if (myModels.containsKey(_model) == false) {
            return DeleteModel.ModelNotFound;
        } else {
            try {
                DataSourceInterface temp = myModels.remove(_model);
                return DeleteModel.Success;
            } catch (Exception e) {
                return DeleteModel.Failure;
            }
        }
    }

    @Override
    public ImportModel ImportModel(String _path, String _modelName, FileFormat _format) {

        Model model = ModelFactory.createDefaultModel();
        InputStream in = FileManager.get().open(_path);
        String format;

        switch (_format) {
            case N3:
                format = "N3";
                break;
            case NTRIPLE:
                format = "N-TRIPLE";
                break;
            case RDF:
                format = "RDF/XML";
                break;
            case RDFABBREV:
                format = "RDF/XML-ABBREV";
                break;
            case TTL:
                format = "TTL";
                break;
            case TURTLE:
                format = "TURTLE";
                break;
            default:
                format = "RDF/XML";
        }

        if (in == null) {
            return ImportModel.FileNotFound;
        } else {
            try {
                model.read(in, null, format);
                in.close();
                LocalGraph newGraph = new LocalGraph(model);
                if (this.myModels.containsKey(_modelName)) {
                    this.myModels.remove(_modelName);
                    this.myModels.put(_modelName, newGraph);
                } else {
                    this.myModels.put(_modelName, newGraph);
                }
                myModels.put(_modelName, newGraph);
            } catch (Exception e) {
                return ImportModel.FileParseError;
            }
            return ImportModel.Success;
        }
    }

    @Override
    public boolean ContainsModel(String _model) {
        if (this.myModels.containsKey(_model) == true) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void ImportGenericHTTPEndPoint(String _URL, String _name) {

        if (!this.myModels.containsKey(_name)) {
            this.myModels.put(_name, new GenericHTTPEndPoint(_URL));
        }
    }

    /**
     *
     * @param _model
     * @return Returns true if model was renamed, false if model not found
     */
    @Override
    public boolean RenameModel(String _model, String _newName) {

        if (myModels.containsKey(_model)) {
            DataSourceInterface temp = myModels.remove(_model);
            myModels.put(_newName, temp);
            return true;
        } else {
            return false;
        }
    }

    /**
     * *
     * Returns URI fragment identifier or las substring after the last '/'
     *
     * @param _URI URI to be parsed
     * @return String with fragment identifier or last section of URI. If
     * invalid URI, return null
     */
    private String getShortName(String _URI) {

        String shortName = null;

        if (_URI.length() > 2) {
            int i;
            for (i = _URI.length() - 1; i >= 0; i--) {
                if (_URI.charAt(i) == '/' || _URI.charAt(i) == '#') {
                    break;
                }
            }
            shortName = _URI.substring(i + 1);
        }

        return shortName;
    }

    /**
     * *
     * Determines the type of SPARQL query
     *
     * @param _query Query for which type is to be determined
     * @return Returns type of query or error
     */
    @Override
    public SPARQLExpressionType getQueryType(String _query) {
        try {
            Query query = QueryFactory.create(_query);
            if (query.isSelectType()) {
                return SPARQLExpressionType.SELECT;
            } else if (query.isConstructType()) {
                return SPARQLExpressionType.CONSTRUCT;
            } else if (query.isAskType()) {
                return SPARQLExpressionType.ASK;
            } else if (query.isDescribeType()) {
                return SPARQLExpressionType.DESCRIBE;
            } else if (query.isUnknownType()) {
                return SPARQLExpressionType.UNKNOWN;
            } else {
                return SPARQLExpressionType.ERROR;
            }
        } catch (Exception e) {
            return SPARQLExpressionType.ERROR;
        }
    }

    @Override
    public SPARQL_ResultSet RunSelect(String _query, String _model) {

        if (this.myModels.containsKey(_model)) {
            SPARQL_ResultSet myResults = this.myModels.get(_model).performSelect(_query);
            return myResults;
        } else {
            return null;
        }
    }

    @Override
    public void RunConstruct(String _query, String _model) {
        if (this.myModels.containsKey(_model)) {
            try {
                Model result = this.myModels.get(_model).performConstruct(_query);
                LocalGraph newGraph = new LocalGraph(result);
                if (this.myModels.containsKey(_model + " subset") == true) {
                    this.myModels.remove(_model + " subset");
                    this.myModels.put(_model + " subset", newGraph);
                } else {
                    this.myModels.put(_model + " subset", newGraph);
                }
            } catch (Exception ex) {
                Logger.getLogger(DataStorage.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     *
     * @param _model Model to get triple statements from. Must be LocalGraph
     * @return List of statements or null if _model is not valid
     */
    @Override
    public ArrayList<TripleStatement> GetStatements(String _model) {
        return this.myModels.get(_model).GetStatements();
    }

    /**
     *
     * @param _model Model to be checked
     * @return Returns true if _model is type LocalGraph
     */
    @Override
    public boolean isLocalGraph(String _model) {
        if (this.myModels.get(_model) instanceof LocalGraph) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean TestConnection(String _modelName) {
        if (this.myModels.containsKey(_modelName) == true) {
            return this.myModels.get(_modelName).verifyConnection();
        } else {
            return false;
        }
    }

    public ReadyToVisualise isReadyToVisualise(String _model) {

        if (this.myModels.containsKey(_model)) {
            return this.myModels.get(_model).ReadyToVisualise();
        } else {
            return ReadyToVisualise.ModelNotFound;
        }
    }

    public VisualiseStatus UpdateModel(String _model) {
        try {
            this.myModels.get(_model).UpdateModel();
        } catch (Exception ex) {
            if (ex.getCause() instanceof SocketTimeoutException) {
                return VisualiseStatus.Timeout;
            } else {
                return VisualiseStatus.UnknownError;
            }
        }
        return VisualiseStatus.Success;
    }

    public VisualiseStatus UpdateModel(String _model, String _newCenter, NodeType _type) {
        try {
            this.myModels.get(_model).UpdateModel(_newCenter, _type);
        } catch (Exception ex) {
            if (ex.getCause() instanceof SocketTimeoutException) {
                return VisualiseStatus.Timeout;
            } else {
                return VisualiseStatus.UnknownError;
            }
        }
        return VisualiseStatus.Success;
    }

    /**
     * Returns the currently selected center vertex for a model
     *
     * @param _model Model to get vertex for
     * @return String of vertex or null if invalid model
     */
    @Override
    public String getCenterVertex(String _model) {

        if (this.myModels.containsKey(_model)) {
            return this.myModels.get(_model).getCenterVertex();
        } else {
            return null;
        }
    }

    @Override
    public void setCentervertex(String _model, String _newVertex, NodeType _type) {
        if (this.myModels.containsKey(_model)) {
            this.myModels.get(_model).setCenterVertex(_newVertex, _type);
        }
    }

    /**
     * Performs an SPARQL Ask query on the designated model
     *
     * @param _model Model to run SPARQL on
     * @param _query Query to be performed
     * @return Returns custom enum with result or error type
     */
    @Override
    public RunAskResult RunAsk(String _query, String _model) {
        RunAskResult result;
        if (myModels.containsKey(_model)) {
            try {
                boolean askResult = myModels.get(_model).performAsk(_query);
                if (askResult == true) {
                    result = RunAskResult.True;
                } else {
                    result = RunAskResult.False;
                }
            } catch (QueryExceptionHTTP e) {
                if (e.getCause() instanceof SocketTimeoutException) {
                    result = RunAskResult.Timeout;
                } else {
                    result = RunAskResult.UnknownError;
                }
            }
        } else {
            result = RunAskResult.ModelNotFound;
        }
        return result;
    }

    @Override
    public ResourceResult UpdateVertexURI(String _model, String _vertex, String _newURI) {
        ResourceResult results;
        if (myModels.containsKey(_model)) {
            results = myModels.get(_model).UpdateVertexURI(_vertex, _newURI);

        } else {
            results = new ResourceResult();
            results.actionReport = UpdateVertexURI.ModelNotFound;
        }
        return results;
    }

    @Override
    public ResourceResult VertexToBlankNode(String _model, String _vertex) {
        ResourceResult results;
        if (myModels.containsKey(_model)) {
            results = myModels.get(_model).VertexToBlankNode(_vertex);
        } else {
            results = new ResourceResult();
            results.actionReport = VertexToBlankNode.ModelNotFound;
        }
        return results;
    }

    @Override
    public ResourceResult ResolveBlankNode(String _model, String _nodeID, String _newURI) {
        ResourceResult results;
        if (myModels.containsKey(_model)) {
            results = myModels.get(_model).ResolveBlankNode(_nodeID, _newURI);
        } else {
            results = new ResourceResult();
            results.actionReport = UpdateVertexURI.ModelNotFound;
        }
        return results;
    }

    @Override
    public DeleteVertex DeleteBlankNode(String _model, String _nodeID) {
        if (myModels.containsKey(_model)) {
            return myModels.get(_model).DeleteBlankNode(_nodeID);
        } else {
            return DeleteVertex.ModelNotFound;
        }
    }

    @Override
    public ResourceResult InsertBlankNode(String _model) {
        ResourceResult results;
        if (myModels.containsKey(_model)) {
            results = myModels.get(_model).InsertBlankNode();
        } else {
            results = new ResourceResult();
            results.actionReport = InsertVertex.ModelNotFound;
        }
        return results;
    }

    @Override
    public UpdateLiteralText UpdateLiteralText(String _model, String _literal, String _newText) {
        if (myModels.containsKey(_model)) {
            return myModels.get(_model).UpdateLiteralText(_literal, _newText);
        } else {
            return UpdateLiteralText.ModelNotFound;
        }
    }

    @Override
    public DeleteLiteral DeleteLiteral(String _model, String _literal) {
        if (myModels.containsKey(_model)) {
            return myModels.get(_model).DeleteLiteral(_literal);
        } else {
            return DeleteLiteral.ModelNotFound;
        }
    }

    @Override
    public InsertLiteral InsertLiteral(String _model, String _text) {
        if (myModels.containsKey(_model)) {
            return myModels.get(_model).InsertLiteral(_text);
        } else {
            return InsertLiteral.ModelNotFound;
        }
    }

    @Override
    public DeleteEdge DeleteEdge(String _model,
            String _edgeURI, String _source,
            NodeType _sourceType, String _target,
            NodeType _targetType) {
        if (myModels.containsKey(_model)) {
            return myModels.get(_model).DeleteEdge(_model, _edgeURI, _source, _sourceType, _target, _targetType);
        } else {
            return DeleteEdge.ModelNotFound;
        }
    }

    @Override
    public ResourceResult ChangeEdgeURI(String _model, String _edgeURI, String _source, NodeType _sourceType, String _target, NodeType _targetType, String _newEdgeURI) {
        ResourceResult result;
        if (myModels.containsKey(_model)) {
            result = myModels.get(_model).ChangeEdgeURI(_edgeURI, _source, _sourceType, _target, _targetType, _newEdgeURI);
        } else {
            result = new ResourceResult();
            result.actionReport = ChangeEdge.ModelNotFound;
        }
        return result;
    }

    @Override
    public ResourceResult InsertEdge(String _model, String _edgeURI, String _source, NodeType _sourceType, String _target, NodeType _targetType) {
        ResourceResult result;
        if (myModels.containsKey(_model)) {
            return null;
        } else {
            result = new ResourceResult();
            result.actionReport = InsertEdge.ModelNotFound;
        }
        return result;
    }

    @Override
    public NodeType getNodeType(String _model, String _value) {
        if (myModels.containsKey(_model)) {
            return myModels.get(_model).getNodeType(_value);
        } else {
            return null;
        }
    }

    @Override
    public ArrayList<TripleStatement> CBD(String _model, String _vertex) {
        ArrayList<TripleStatement> result = new ArrayList<>();
        if (myModels.containsKey(_model)) {
            result = myModels.get(_model).CBD(_vertex);
        } else {

        }
        return result;
    }

    @Override
    public ArrayList<TripleStatement> SCBD(String _model, String _literal) {
        ArrayList<TripleStatement> result = new ArrayList<>();
        if (myModels.containsKey(_model)) {
            result = myModels.get(_model).SCBD(_literal);
        }
        return result;
    }

    @Override
    public TripleStatementWrapper InterestingSearch(String _model, String _bagOfWords, int _maxNodes, int _maxDist, String _initNode, NodeType _initType, double _beta, String[] _filters, InterestingGraphThread _report, String _lang) {
        TripleStatementWrapper result = new TripleStatementWrapper();
        if (myModels.containsKey(_model)) {
            result = myModels.get(_model).InterestingSearch(_bagOfWords, _maxNodes, _maxDist, _initNode, _initType, _beta, _filters, _report, _lang);
        } else {
            result.actionReport = InterestingResult.ModelNotFound;
        }
        return result;
    }

    @Override
    public InterestingLiterals getLiterals(String _model, String _center, NodeType _centerType) {
        InterestingLiterals result = new InterestingLiterals();

        if (myModels.containsKey(_model)) {
            result = myModels.get(_model).getLiterals(_center, _centerType);
        } else {
            result.actionReport = InterestingResult.ModelNotFound;
        }
        return result;
    }

    @Override
    public void setMaxVertices(String _model, int _max) {
        if (myModels.containsKey(_model)) {
            myModels.get(_model).setMaxVertices(_max);
        }
    }

    @Override
    public int getMaxVertices(String _model) {
        if (myModels.containsKey(_model)) {
            return myModels.get(_model).getMaxVertices();
        } else {
            return -1;
        }
    }

    /**
     * Generates a sub-model using a BFS centered on a node provided,
     * for a specific model. 
     * The sub-model is returned in the form TripleStatements 
     * for visualisation.
     * @param _model Model to create sub-model for
     * @param _center New center node
     * @param _type Type of new center node
     * @return Returns VisualiseWrapper containing process report and triples
     */
    @Override
    public VisualiseWrapper CenterOn(String _model, String _center, NodeType _type) {
        VisualiseWrapper result;
        if (myModels.containsKey(_model)) {
            result = myModels.get(_model).CenterOn(_center, _type);
        } else {
            result = new VisualiseWrapper(null, VisualiseStatus.UnableToFindModel);
        }
        return result;
    }

    /**
     * Generates a sub-model using a BFS centered on the current center vertex
     * for a specific model. The sub-model is returned in the form TripleStatements
     * for visualisation.
     * @param _model Model to generate sub-model for
     * @return Returns VisualiseWrapper containing process report and triples
     */
    @Override
    public VisualiseWrapper CenterOn(String _model) {
        VisualiseWrapper result;
        if (myModels.containsKey(_model)) {
            result = myModels.get(_model).CenterOn();
        } else {
            result = new VisualiseWrapper(null, VisualiseStatus.UnableToFindModel);
        }
        return result;
    }

    @Override
    public DisambiguityResults DisambiguitySearch(String _model, String _searchTerm, String _lang) {
        DisambiguityResults result;
        if (myModels.containsKey(_model)) {
            result = myModels.get(_model).DisambiguitySearch(_searchTerm, _lang);
        } else {
            result = new DisambiguityResults(InterestingResult.ModelNotFound, null);
        }
        return result;
    }

    @Override
    public InterestingPath[] InterestingPaths(String _model, String[] _NSFilters, String _source, String _target, String _searchTerms, String _lang, int _minPath, int _maxPath, int _maxNumOfPaths, InterestingPathsThread _report) {
        if(myModels.containsKey(_model)) {
            return myModels.get(_model).InterestingPaths(_NSFilters, _source, _target, _searchTerms, _lang, _minPath, _maxPath, _maxNumOfPaths, _report);
        }
        else {
            return null;
        }
    }

    @Override
    public void setTimeout(String _model, int _timeout) {
        if(myModels.containsKey(_model)) {
            myModels.get(_model).setTimeout(_timeout);
        }
    }

    @Override
    public int getTimeout(String _model) {
        if(myModels.containsKey(_model)) {
            return myModels.get(_model).getTimeout();
        }
        else {
            return -1;
        }
    }

    @Override
    public String getEndpointURI(String _model) {
        if(myModels.containsKey(_model)) {
            return myModels.get(_model).getEndpointURL();
        }
        else {
            return "";
        }
    }

    @Override
    public void setEndpointURI(String _model, String _newURI) {
        if(myModels.containsKey(_model)) {
            myModels.get(_model).setEndpointURL(_newURI);
        }
    }
}
