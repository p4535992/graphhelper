/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import java.util.LinkedList;
import java.util.List;

public class InformationReporter {

    List<IterationData> myData = new LinkedList<>();
    public double MemoryIntitial = 0.0;
    public IterationData current;

    public InformationReporter(int _itr) {
        current = new IterationData(_itr);
    }

    public void Next() {
        int nextStep = current.Iteration + 1;
        myData.add(current);
        current = new IterationData(nextStep);
    }

    public void Print() {
        for (IterationData itr : myData) {
            System.out.println(itr.toString());
        }
    }
}
