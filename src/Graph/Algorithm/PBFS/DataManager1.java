/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import gnu.trove.list.TIntList;
import gnu.trove.set.hash.THashSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class DataManager1 {

    private final int maxResultset = 10000;
    private final int maxPackage = 2000;

    private final String[] myFilters;

    public THashSet<String> myBlacklist = new THashSet<>();
    private List<InformedNode> myQueue = new ArrayList<>();
    private THashSet<String> myQueuePlusOne = new THashSet<>();
    private int myMetaPtr = 0;
    private int myQueueSize;

    private List<Bin> myBins = new ArrayList<>();
    private int myBinPtr = 0;
    private InformationReporter myReport;

    /**
     * For new sub-graph construction
     *
     * @param _seed Initial URI seed
     * @param _filters Filters to be used
     */
    public DataManager1(String _seed, String[] _filters) {
        myBlacklist.add(_seed);
        myQueue.add(new InformedNode(_seed, 0));
        myQueueSize = myQueue.size();
        myFilters = _filters;
    }

    public DataManager1(String _seed, String[] _filters, InformationReporter _iReport) {
        myBlacklist.add(_seed);
        myQueue.add(new InformedNode(_seed, 0));
        myQueueSize = myQueue.size();
        myFilters = _filters;
        myReport = _iReport;
    }

    /**
     * Restores a data manager in order to extend a graph
     *
     * @param _frontier Frontier to start from
     * @param _filters Filters to be used
     */
    public DataManager1(List<String> _frontier, String[] _filters) {
        for (String s : _frontier) {
            myBlacklist.add(s);
            myQueue.add(new InformedNode(s, 0));
        }
        myQueueSize = myQueue.size();
        myFilters = _filters;
    }

    /**
     * Queues and sets to null and Garbage Collector called
     */
    public void Cleanup() {
        myBlacklist = null;
        myQueue = null;
        System.gc();
    }

    /**
     * Returns a MetaWorkUnit, or null if the queue is empty
     *
     * @return MetaWorkUnit or null
     */
    public synchronized MetaWorkUnit1 getMetaWorkUnit() {
        MetaWorkUnit1 mwu = null;
        if (myMetaPtr < myQueueSize) {
            if ((myMetaPtr + maxPackage) < myQueueSize) {
                List<InformedNode> subList = myQueue.subList(myMetaPtr, myMetaPtr + maxPackage);
                mwu = new MetaWorkUnit1(myFilters, subList);
                myMetaPtr += maxPackage;
                myReport.current.metaUnitCount++;
            } else {
                List<InformedNode> subList = myQueue.subList(myMetaPtr, myQueueSize);
                mwu = new MetaWorkUnit1(myFilters, subList);
                myMetaPtr += subList.size();
                myReport.current.metaUnitCount++;
            }
        }
        return mwu;
    }

    /**
     * Returns a DataWorkUnit or null if queue is empty
     *
     * @return DataWorkUnit to be processed
     */
    public synchronized DataWorkUnit1 getDataWorkUnit() {
        if (myBinPtr < myBins.size()) {
            Bin temp = myBins.get(myBinPtr);
            List<String> URIs = new ArrayList<>(temp.getCount());

            TIntList binMembers = temp.getMembers();
            for (int i = 0; i < binMembers.size(); i++) {
                URIs.add(myQueue.get(binMembers.get(i)).myURI);
            }

            DataWorkUnit1 dwu = new DataWorkUnit1(myFilters, URIs, myBlacklist);

            // Moving pointer
            myBinPtr++;

            return dwu;
        } else {
            return null;
        }
    }

    /**
     * Returns DataWorkUnit, where the URIs will be added to the frontier
     *
     * @param _dwu Processed DataWorkUnit returned
     */
    public synchronized void putDataWorkUnit(DataWorkUnit1 _dwu) {
        this.myQueuePlusOne.addAll(_dwu.getFrontierCandidates());
    }

    /**
     * Moves the frontier, and makes a new frontier+1. Generates new black list
     * as well.
     */
    public void MoveFrontier() {
        // New frontier becomes current blacklist
        myBlacklist = myQueuePlusOne;

        // Clearing out queue
        myQueue.clear();

        // Adding current frontiers URIs to the queue
        for (String s : myBlacklist) {
            myQueue.add(new InformedNode(s, 0));
        }

        // Resetting meta information
        myMetaPtr = 0;
        myQueueSize = myQueue.size();

        // Resetting next queue
        myQueuePlusOne = new THashSet<>(myBlacklist.size());

        // Reseeting bins
        myBins.clear();
        myBinPtr = 0;
    }

    /**
     * Returns the current frontier of URI strings
     *
     * @return Returns set of URIs as strings
     */
    public Set<String> getFrontier() {
        return this.myBlacklist;
    }

    public void JuggleBins() {
        Collections.sort(myQueue);

        // Calculating theoretical optimal
        long sum = 0;
        for (InformedNode in : this.myQueue) {
            if (in.myResultSize < maxResultset) {
                sum += in.myResultSize;
            }
        }
        myReport.current.optimalNumOfPackages = (((double) sum) / ((double) maxResultset));
        BestFitFirstDescenting();
    }

    private void BestFitFirstDescenting() {
        if (myBins.isEmpty()) {
            myBins.add(new Bin());
        }

        // Skipping massive units. We will add them later
        int ptr = 0;
        while (myQueue.get(ptr).myResultSize > maxResultset) {
            ptr++;
        }

        for (int i = ptr; i < myQueue.size(); i++) {
            boolean inBin = false;
            for (Bin b : myBins) {
                if ((b.getVolume() + myQueue.get(i).myResultSize) < maxResultset
                        && b.getCount() < maxPackage) {
                    b.add2Bin(i, myQueue.get(i).myResultSize);
                    inBin = true;
                }
            }
            if (inBin == false) {
                Bin newBin = new Bin();
                newBin.add2Bin(i, myQueue.get(i).myResultSize);
                myBins.add(newBin);
            }
        }
        myReport.current.dataUnitCount = myBins.size();

        // Adding in massive packages
        ptr = 0;
        while (myQueue.get(ptr).myResultSize > maxResultset) {
            Bin temp = new Bin();
            temp.add2Bin(ptr, myQueue.get(ptr).myResultSize);
            ptr++;
        }
        
        for(Bin b : myBins) {
            myReport.current.dataUnitSizes.add(b.getCount());
        }
    }
}
