/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;

public class MetaWorker extends Thread {

    private final String myEndpoint;
    private final DataManager myDM;
    private MetaWorkUnit myUnit = null;

    private String myQuery;
    private Query myQueryObj;
    private QueryExecution qExe;
    private ResultSet rs;

    public MetaWorker(String _endpoint, DataManager _dm) {
        myEndpoint = _endpoint;
        myDM = _dm;
    }

    @Override
    public void run() {
        myUnit = myDM.getMetaWorkUnit();

        while (myUnit != null) {
            myQuery = myUnit.getQuery();

            try {
                // Creating query object
                myQueryObj = QueryFactory.create(myQuery);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                myQueryObj = null;
            }

            // Retrieving data
            RetrieveData();

            // Handing off workunit
            myDM.putMetaWorkUnit(myUnit);

            // Requesting new MetaWorkUnit
            myUnit = myDM.getMetaWorkUnit();
        }
    }

    private void RetrieveData() {
        int counter = 3;

        if (myQueryObj != null) {
            while (counter > 0) {
                try {
                    // Constructing query plan
                    qExe = QueryExecutionFactory.sparqlService(myEndpoint, myQueryObj);

                    // Executing select
                    rs = qExe.execSelect();

                    // Parsing results
                    myUnit.parseResultSet(rs);

                    // Closing connection
                    qExe.close();

                    // We finished without error
                    counter = 0;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    counter--;
                }
            }
        }
    }
}
