/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;

public class DataWorker extends Thread {

    // <editor-fold defaultstate="collapsed" desc="Properties">
    private final String myEndpoint;
    private final DataManager myDM;
    private DataWorkUnit myUnit = null;

    private String myQuery;
    private Query myQueryObj;
    private QueryExecution qExe;
    private ResultSet rs;

    private TDB myTDB;
    
    private int safetyCounter = 0;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Constructor">
    public DataWorker(String _endpoint, DataManager _DM, TDB _TDB) {
        myEndpoint = _endpoint;
        myDM = _DM;
        myTDB = _TDB;
    }
    // </editor-fold>

    @Override
    public void run() {
        myUnit = myDM.getDataWorkUnit();
        
        // Keep processing work units till we get a null which means empty queue
        while (myUnit != null) {
            myQuery = myUnit.getQuery();

            while (myQuery != null && safetyCounter < 9) {
                safetyCounter++;
                try {
                    // Creating query object
                    myQueryObj = QueryFactory.create(myQuery);
                } catch (Exception e) {
                    myQueryObj = null;
                }

                if (myQueryObj != null) {
                    try {
                        // Constructing query plan
                        qExe = QueryExecutionFactory.sparqlService(myEndpoint, myQueryObj);

                        // Executing select
                        rs = qExe.execSelect();

                        // Parsing results
                        myUnit.parseResultSet(rs);

                        // Closing connection
                        qExe.close();
                    } catch (Exception e) {
                    }
                }

                // Creating next query
                myQuery = myUnit.getQuery();
            }

            // Handing off statements
            myTDB.InsertStatements(myUnit.getStatementsFound());

            // Handing off workunit
            myDM.putDataWorkUnit(myUnit);

            // Requesting new DataWorkUnit
            myUnit = myDM.getDataWorkUnit();
            safetyCounter = 0;
        }
    }
}
